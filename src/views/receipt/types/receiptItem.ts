import type { Product } from '@/views/product/types/Product'

type ReceiptItem = {
  id: number
  name: string
  price: number
  unit: number
  total: number
  productId: number
  product?: Product
  receiptId?: number
}

export { type ReceiptItem }
