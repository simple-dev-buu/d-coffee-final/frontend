import type { Branch } from '@/views/Branch/types/branch'
import type { Customer } from '@/views/customer/types/Customer'
import type { Employee } from '@/views/employee/types/Employee'
import type { Promotion } from '@/views/promotion/types/Promotion'
import type { ReceiptItem } from '@/views/receipt/types/receiptItem'

export type PaymentType = 'cash' | 'credit' | 'promptPay'

export const defaultReceipt: Receipt = {
  totalUnit: 0,
  totalBefore: 0,
  discount: 0,
  netPrice: 0,
  receivedAmount: 0,
  change: 0,
  paymentType: 'cash',
  givePoint: 0,
  usePoint: 0,
  totalPoint: 0,
  createdDateTime: new Date(),
  empId: 0,
  branchId: 0,
  cusId: 0,
  receiptItems: []
}

export type Receipt = {
  id?: number
  totalUnit: number
  totalBefore: number
  discount: number
  netPrice: number
  receivedAmount: number
  change: number
  paymentType: PaymentType
  givePoint: number
  usePoint: number
  totalPoint: number
  createdDateTime: Date
  empId: number
  branchId: number
  cusId: number
  receiptItems?: ReceiptItem[]
  employee?: Employee
  customer?: Customer
  branch?: Branch
  promotionId?: number
  promotion?: Promotion
}
