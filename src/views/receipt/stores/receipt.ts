import { ref, watch } from 'vue'
import type { Receipt } from '../types/receipt'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '../types/receiptItem'
import orderService from '@/services/order'
// import { useCustomerStore } from '@/views/customer/stores/customer'
import { useMessageStore } from '@/stores/message'
import { useMemberStore } from '@/views/customer/stores/member'
import { useAuthStore } from '@/stores/auth'
import { generatePDF } from '@/utils/pdf'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  // const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const memberStore = useMemberStore()
  const receiptItems = ref<ReceiptItem[]>([])
  const receipt = ref<Receipt>()
  const receiptDialog = ref(false)
  const dialogPromptPay = ref(false)
  const dialogCredit = ref(false)
  const errorInput = ref(false)
  const errorInputPoint = ref(false)
  initReceipt()
  function initReceipt() {
    receipt.value = {
      totalUnit: 0,
      totalBefore: 0,
      discount: 0,
      netPrice: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      givePoint: 0,
      usePoint: 0,
      totalPoint: 0,
      createdDateTime: new Date(),
      // total: 0, //baht
      // amount: 0, //unit
      // change: 0,
      branchId: -1,
      empId: -1,
      // userId: authStore.getCurrentUser()?.id!,
      cusId: -1
      // user: authStore.getCurrentUser()!
    }
    receiptItems.value = []
  }
  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )
  const exportPDF = () => {
    const pdfHeaders = ['ID', 'Name', 'Price', 'Unit', 'Total']
    const pdfData = receiptItems.value!.map((item) => [
      item.id as unknown as string,
      item.name as string,
      item.price as unknown as string,
      item.unit as unknown as string,
      (item.unit * item.price) as unknown as string
    ])
    generatePDF(`Receipt`, pdfData, pdfHeaders, 'receipt')
  }

  const calReceipt = function () {
    receipt.value!.totalBefore = 0
    receipt.value!.totalUnit = 0
    for (let i = 0; i < receiptItems.value.length; i++) {
      receipt.value!.totalBefore += receiptItems.value[i].price * receiptItems.value[i].unit
      receipt.value!.totalUnit += receiptItems.value[i].unit
      receipt.value!.netPrice = receipt.value!.totalBefore - receipt.value!.discount
      calPoint()
    }
  }

  function calMoney(money: number) {
    if (money > 0) {
      receipt.value!.receivedAmount = money
      receipt.value!.change = money - receipt.value!.netPrice
    } else {
      errorInput.value = true
    }
  }

  function usePoints(use: number) {
    if (use <= receipt.value!.totalPoint) {
      receipt.value!.usePoint = use
      receipt.value!.totalPoint = receipt.value!.totalPoint - use
      console.log('Total Point' + receipt.value!.totalPoint)
      console.log('Point ที่ใช้ ' + use)
      console.log('usePoint ที่ใช้ ' + receipt.value!.usePoint)

      // console.log(promotionStore.currentPromotion?.name)

      //if use point
      receipt.value!.usePoint = use
      receipt.value!.discount = use
      console.log('Discount ' + use)
      receipt.value!.netPrice = receipt.value!.totalBefore - receipt.value!.discount

      // calReceipt()
    } else {
      errorInputPoint.value = true
    }
  }

  function calPoint() {
    let givePoint = 0
    // let usePoints = 0
    let totalPoint = 0
    let memberPoint = 0
    for (const item of receiptItems.value) {
      givePoint = givePoint + item.unit
      totalPoint = givePoint
    }
    if (memberStore.currentMember != null) {
      memberPoint = memberStore.currentMember.point
      receipt.value!.totalPoint = receipt.value!.givePoint + memberStore.currentMember.point
      console.log(receipt.value!.totalPoint)
    }
    receipt.value!.givePoint = givePoint
    // receipt.value!.usePoint = usePoint
    receipt.value!.totalPoint = totalPoint + memberPoint
  }

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    console.log(newReceiptItem)
    //check id product
    let notdup = false
    if (receiptItems.value.length == 0) {
      receiptItems.value.push(newReceiptItem)
      return
    } else {
      for (const p of receiptItems.value) {
        if (p.productId == newReceiptItem.productId) {
          p.unit = p.unit + 1

          return
        }
        notdup = true
      }
      if (notdup == true) {
        receiptItems.value.push(newReceiptItem)
        return
      }
    }

    console.log(newReceiptItem)
  }

  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
    console.log(incUnitOfReceiptItem)
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      deleteReceiptItem(selectedItem)
    }
    console.log(decUnitOfReceiptItem)
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
  }
  const savePayment = async () => {
    try {
      // loadingStore.doLoad()
      if (memberStore.currentMember?.id) {
        receipt.value!.cusId = memberStore.currentMember?.id
      } else {
        receipt.value!.cusId = -1
      }
      // exportPDF()
      console.log(receipt.value?.cusId)
      await orderService.addOrder(receipt.value!, receiptItems.value)
      // // exportPDF()
      // console.log(receipt.value?.memberId)
      // await orderService.addOrder(receipt.value!, receiptItems.value)
      initReceipt()
      // loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      // loadingStore.finish()
      memberStore.clear()
    }
  }
  const saveExportPDF = async () => {
    // exportPDF()
    await orderService.addOrder(receipt.value!, receiptItems.value)
    initReceipt()
  }
  const clear = function () {
    receiptItems.value = []
    receipt.value = Object.assign({
      // createdDate: new Date(),
      // total: 0, //baht
      // amount: 0, //unit
      // change: 0,
      // paymentType: 'Cash',
      // userId: 0,
      // memberId: 0,
      // user: 0

      totalUnit: 0,
      totalBefore: 0,
      discount: 0,
      netPrice: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'Cash',
      givePoint: 0,
      usePoint: 0,
      totalPoint: 0,
      createdDateTime: new Date(),
      userId: 0,
      memberId: 0,
      user: 0
    })
    console.log(receipt.value)
  }

  // function toEnablePromotion() {
  //   receiptItems.value.forEach((item) => {
  //     item.productId
  //   })
  // }

  const paymentType = ref()
  let queue = 1
  const showReceiptDialog = function () {
    receipt.value!.paymentType = paymentType.value
    console.log(queue)
    queue = queue + 1
    console.log(queue)
    if (
      receipt.value!.change >= 0 &&
      receipt.value!.totalBefore > 0 &&
      receipt.value!.paymentType === 'cash' &&
      receiptItems.value.length > 0
    ) {
      errorInput.value = false
      receiptDialog.value = true
      receipt.value!.receiptItems = receiptItems.value
      receipt.value!.paymentType = paymentType.value
    } else if (receipt.value!.paymentType === 'promptPay') {
      dialogPromptPay.value = true
      receipt.value!.receivedAmount = receipt.value!.netPrice
      receipt.value!.receiptItems = receiptItems.value
      receipt.value!.paymentType = paymentType.value
    } else if (receipt.value!.paymentType === 'credit') {
      dialogCredit.value = true
      receipt.value!.receivedAmount = receipt.value!.netPrice
      receipt.value!.receiptItems = receiptItems.value
      receipt.value!.paymentType = paymentType.value
    } else {
      // errorInputPoint.value = true
    }
  }
  return {
    receipt,
    receiptItems,
    receiptDialog,
    dialogCredit,
    dialogPromptPay,
    errorInput,
    errorInputPoint,
    usePoints,
    calMoney,
    clear,
    calReceipt,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    removeItem,
    savePayment,
    showReceiptDialog,
    saveExportPDF
  }
})
