import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { Role } from '../types/Role'

export const useRoleStore = defineStore('role', () => {
  const roles: Role[] = [
    { id: 1, name: 'Admin' },
    { id: 2, name: 'User' }
  ]

  return {
    roles
  }
})
