type Order = {
  // orderItems: {
  // productId?: number
  // qty: number
  // // }[]
  // userId?: number
  // branchId?: number
  // customerid?: number

  id?: number
  totalUnit: number
  totalBefore: number
  discount: number
  netPrice: number
  receivedAmount: number
  change: number
  paymentType: string
  givePoint: number
  usePoint: number
  totalPoint: number
  createdDateTime: Date
}

export { type Order }
