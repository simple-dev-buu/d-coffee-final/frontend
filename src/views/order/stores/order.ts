import { defineStore } from 'pinia'
import { ref } from 'vue'

import type { Order } from '../types/Order'
import order from '@/services/order'
import receiptService from '@/services/receiptold'
import type { Receipt } from '@/views/receipt/types/receipt'
import { generatePDF } from '@/utils/pdf'

export const useOrderStore = defineStore('order', () => {
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Total Unit', key: 'totalUnit', sortable: false },
    { title: 'Total Before', key: 'totalBefore', sortable: false },
    { title: 'Discount', key: 'discount', sortable: false },
    { title: 'Net Price', key: 'netPrice', sortable: false },
    { title: 'Received Amount', key: 'receivedAmount', sortable: false },
    { title: 'Change', key: 'change', sortable: false },
    { title: 'Payment Type', key: 'paymentType', sortable: false },
    { title: 'Give Point', key: 'givePoint', sortable: false },
    { title: 'Use Point', key: 'usePoint', sortable: false },
    { title: 'Total Point', key: 'totalPoint', sortable: false },
    { title: 'Created Date/Time', key: 'createdDateTime', sortable: false }
  ]
  const moneyNeed = ref(0)
  const messageMoney = ref('')
  const items = ref<Order[]>([])
  const initialOrder = ref()
  const orderItem = ref<Order & { files: File[] }>(JSON.parse(JSON.stringify(initialOrder)))
  const receiptItem = ref<Receipt>()
  const dialog = ref(false)

  const openDialog = () => {
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }

  const getAll = async () => {
    // loadingStore.doLoad()
    items.value = await order.getAll()
    console.log(items.value)
    // loadingStore.finishLoad()
  }

  const getMoreOrLess = async (choice: string, branchId: number) => {
    if (choice == 'มากกว่า') {
      const data = await receiptService.getMore(moneyNeed.value, branchId)
      messageMoney.value =
        'จำนวนใบเสร็จที่มูลค่ามากกว่า ' +
        moneyNeed.value +
        ' บาทของสาขานี้คือ ' +
        Object.values(data[0][0]) +
        ' ใบเสร็จ'
    } else {
      const data = await receiptService.getLess(moneyNeed.value, branchId)
      messageMoney.value =
        'จำนวนใบเสร็จที่มูลค่าน้อยกว่า ' +
        moneyNeed.value +
        ' บาทของสาขานี้คือ ' +
        Object.values(data[0][0]) +
        ' ใบเสร็จ'
    }
    alert(messageMoney.value)
  }

  async function save() {
    try {
      // loadingStore.doLoad()
      const order = orderItem.value
      closeDialog()
      // loadingStore.finish()
    } catch (e: any) {
      // messageStore.showMessage(e.message)
      // loadingStore.finish()
    }
  }
  const resetItem = () => {
    orderItem.value = JSON.parse(JSON.stringify(initialOrder))
  }
  const editItem = async (item: Order) => {
    // openDialog()
  }

  const deleteItem = async (item: Order) => {}

  function downloadPDF() {
    const headers = [
      'ID',
      'Net Price',
      'Payment Type',
      'Create Date/Time'
    ]
    const data = items.value.map((item) => [
      item.id as unknown as string,
      item.netPrice ?? '-',
      item.paymentType ?? '-',
      item.createdDateTime ?? '-'
    ])
    console.log(data)
    generatePDF('Receipt History', data, headers, 'receipt-history')
  }

  return {
    headers,
    items,
    dialog,
    orderItem,
    moneyNeed,
    messageMoney,
    openDialog,
    editItem,
    deleteItem,
    closeDialog,
    save,
    getAll,
    getMoreOrLess,
    receiptItem,
    downloadPDF
  }
})
