export type Customer = {
  // id?: number
  // name: string
  // telephone: string
  // registerDate?: Date
  // birthDay: Date
  // point?: number

  id?: number
  name: string
  tel: string
  birthDay: Date | null
  image: string
  point: number
  registerDate: Date | null
  status: boolean
}

function getImageUrl(customer: Customer) {
  return `http://localhost:3000/images/customers/${customer.image}`
}

export type { getImageUrl }
