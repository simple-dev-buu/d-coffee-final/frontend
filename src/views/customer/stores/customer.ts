import { defineStore } from 'pinia'
import { ref } from 'vue'

import type { Customer } from '@/views/customer/types/Customer'
import { CustomersService } from '@/services/customer'
import { useMessageStore } from '@/stores/message'

export const useCustomerStore = defineStore('customer', () => {
  const customersService = new CustomersService()
  const headers = [
    { title: 'ID', key: 'id', sortable: true },
    { title: 'Image', key: 'image', sortable: false },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Tel', key: 'tel', sortable: false },
    { title: 'BirthDay', key: 'birthDay', sortable: false },
    { title: 'Point', key: 'point', sortable: false },
    { title: 'Register', key: 'registerDate', sortable: false },
    { title: 'Status', key: 'status', sortable: false }
  ]

  const items = ref<Customer[]>([])
  const initialCustomer: Customer & { files: File[] } = {
    name: '',
    tel: '',
    birthDay: null,
    point: 0,
    registerDate: new Date(),
    status: true,
    image: 'noimage.jpg',

    files: []
  }
  const customerItem = ref<Customer & { files: File[] }>(
    JSON.parse(JSON.stringify(initialCustomer))
  )
  const dialog = ref(false)
  const checkDialog = ref(false)

  const openDialog = () => {
    dialog.value = true
    console.log(customerItem.value)
  }
  const openDialogCheck = () => {
    checkDialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }

  const closeDialogCheck = () => {
    checkDialog.value = false
  }

  async function getCustomer(id: number) {
    try {
      // loadingStore.doLoad()
      const res = await customersService.getCustomer(id)
      customerItem.value = res.data
      // loadingStore.finish()
    } catch (e: any) {
      // loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }

  const resetItem = () => {
    customerItem.value = JSON.parse(JSON.stringify(initialCustomer))
  }

  const getAll = async () => {
    items.value = await customersService.getAll()
  }

  async function save() {
    try {
      // loadingStore.doLoad()
      const customer = customerItem.value
      console.log('id' + customer.id)
      if (!customer.id) {
        // Add new
        const res = await customersService.save(customer)
        if (res) useMessageStore().showMessage('Member register success', 'success')
        // const res = await customersService.save(customer)
        // console.log(res.data)
        // console.log('Insert Succesful')
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(customer))
        await customersService.update(customer)
      }
      await getAll()
      closeDialog()
      // loadingStore.finish()
    } catch (e: any) {
      // messageStore.showMessage(e.message)
      // loadingStore.finish()
    } finally {
      resetItem()
    }
  }

  const editItem = async (item: Customer) => {
    customerItem.value.id = item.id
    customerItem.value.tel = item.tel
    customerItem.value.name = item.name
    customerItem.value.birthDay = item.birthDay
    customerItem.value.status = item.status
    console.log(customerItem)
    openDialog()

    if (item.id) {
      await customersService.getCustomer(item.id)
      openDialog()
      console.log(getCustomer(item.id))
    }
  }

  const deleteItem = async (item: Customer) => {
    if (item.id) {
      await customersService.delete(item.id)
      getAll()
    }
  }

  return {
    checkDialog,
    headers,
    items,
    customerItem,
    dialog,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    openDialogCheck,
    closeDialogCheck,
    save,
    resetItem,
    getAll
  }
})

// import { defineStore } from 'pinia'
// import { computed, reactive, ref } from 'vue'
// import type { Customer } from '@/views/customer/types/Customer'
// import CustomerService from '@/services/customer'

// export const useCustomerStore = defineStore('customer', () => {
//   //   const loadingStore = useLoadingStore()
//   const service = new CustomerService()
//   const dialogState = ref(false)
//   const tempItem = reactive<Customer>({
//     name: '',
//     tel: '',
//     birthDay: new Date('2004-28-05'),
//     image: '',
//     point: 0,
//     registerDate: new Date(),
//     status: true
//   })
//   const titleDialog = computed(() => (tempItem.id ? 'Edit' : 'New'))
//   const items = ref<Customer[]>([])
//   const currentMember = ref<Customer | null>()
//   const headers = [
//     {
//       title: 'ID',
//       key: 'id'
//     },
//     {
//       title: 'Name',
//       key: 'name'
//     },
//     {
//       title: 'Tel',
//       key: 'tel'
//     },
//     {
//       title: 'Register Day',
//       key: 'registerDate'
//     },
//     {
//       title: 'Birth Day',
//       key: 'birthDay'
//     },
//     {
//       title: 'Point',
//       key: 'point'
//     },

//     {
//       title: 'Status',
//       key: 'status'
//     }
//   ]

//   const openDialog = () => {
//     dialogState.value = true
//   }
//   const closeDialog = () => {
//     resetItem()
//     dialogState.value = false
//   }
//   const save = async () => {
//     if (tempItem.id) {
//       await service.update(tempItem)
//     } else {
//       const res = await service.saveForPosPage(tempItem)
//       alert(Object.values(res[0]))
//     }
//     closeDialog()
//     getAll()
//   }
//   const resetItem = () => {
//     tempItem.name = ''
//     tempItem.tel = ''
//     ;(tempItem.birthDay = new Date('2004-28-05')),
//       (tempItem.point = 0),
//       (tempItem.image = ''),
//       (tempItem.registerDate = new Date()),
//       (tempItem.status = true)
//   }
//   const getAll = async () => {
//     // loadingStore.doLoad()
//     try {
//       items.value = await service.getAll()
//     } catch (e: any) {
//       console.log(e)
//     }
//     // loadingStore.finishLoad()
//   }
//   const editItem = (item: Customer) => {
//     const tempDateString = item.birthDay.toString()
//     const tempDateRegisString = item.registerDate.toString()
//     tempItem.id = item.id
//     tempItem.name = item.name
//     tempItem.tel = item.tel
//     tempItem.birthDay = new Date(tempDateString)
//     tempItem.point = item.point
//     tempItem.image = item.image
//     tempItem.registerDate = new Date(tempDateRegisString)
//     tempItem.status = item.status
//     openDialog()
//   }
//   const deleteItem = async (item: Customer) => {
//     if (item.id) {
//       await service.delete(item.id)
//       getAll()
//     }
//   }
//   return {
//     titleDialog,
//     editItem,
//     deleteItem,
//     headers,
//     items,
//     customerItem: tempItem,
//     dialogState,
//     openDialog,
//     closeDialog,
//     save,
//     resetItem,
//     getAll
//   }
// })
