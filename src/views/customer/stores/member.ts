import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Customer } from '@/views/customer/types/Customer'
import { useReceiptStore } from '@/views/receipt/stores/receipt'
import { CustomersService } from '@/services/customer'

export const useMemberStore = defineStore('member', () => {
  const receiptStore = useReceiptStore()
  const customerService = new CustomersService()

  const members = ref<Customer[]>([])
  const currentMember = ref<Customer>()
  // const initMember = [{}]
  const getMemberByTel = (tel: string): Customer | null => {
    for (const member of members.value) {
      if (member.tel === tel) return member
    }
    return null
  }
  const searchMember = async (tel: string) => {
    // const index = members.value.findIndex((item) => item.telephone === tel)
    // console.log(members)
    // if (index < 0) {
    //   currentMember.value = null
    // }
    // currentMember.value = members.value[index]
    // receiptStore.receipt.member = currentMember.value
    currentMember.value = await customerService.getByTel(tel)
  }
  function clear() {
    currentMember.value = undefined
  }
  // function showMemberDialog() {
  //   memberDialog.value = true
  //   // currentMember.value
  //   // receipt.value.receiptItem = receiptItems.value
  // }

  return { members, currentMember, searchMember, clear, getMemberByTel }
})
