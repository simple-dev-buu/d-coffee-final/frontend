import { useAuthStore } from '@/stores/auth'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { CusHome } from '../type/customerHome'
import { CustomersService } from '@/services/customer'

export const useCustomerHomeStore = defineStore('profile', () => {
  const authStore = useAuthStore()
  const customerService = new CustomersService()
  const dialog = false

  const initCustomer: CusHome & { files: File[] } = {
    name: '',
    tel: '',
    birthDay: new Date(),
    image: '',
    point: 0,
    registerDate: new Date(),
    files: []
  }
  const customerHome = ref<CusHome & { files: File[] }>(JSON.parse(JSON.stringify(initCustomer)))

  function openCusHome() {
    getCusHome()
  }
  async function getCusHome() {
    const getUser = authStore.getCurrentUser()
    if (getUser?.cusId) {
      const respond = await customerService.getCustomer(getUser.cusId)
      const user = respond.data
      customerHome.value.name = user.name
      customerHome.value.tel = user.receipt
      customerHome.value.birthDay = user.birthDay
      customerHome.value.image = user.image
      customerHome.value.point = user.point
      customerHome.value.registerDate = user.registerDate
    }
  }

  return {
    customerHome,
    dialog,
    openCusHome,
    getCusHome
  }
})
