import { defineStore } from 'pinia'

export const usepictureStore = defineStore('picture', () => {
  const colors = [
    'indigo',
    'warning',
    'pink darken-2',
    'red lighten-1',
    'deep-purple accent-4',
    'blue'
  ]
  const slides = [
    'src/views/CustomerHome/image/PROMOTION.png',
    'src/views/CustomerHome/image/PROMOTION1.png',
    'src/views/CustomerHome/image/PROMOTION2.png',
  ]
  const pictures = [
    'src/views/CustomerHome/image/PROMOTION.png',
    'src/views/CustomerHome/image/PROMOTION.png',
    'src/views/CustomerHome/image/PROMOTION.png',
    'src/views/CustomerHome/image/PROMOTION.png',
    'src/views/CustomerHome/image/PROMOTION.png',
    'src/views/CustomerHome/image/PROMOTION.png'
  ]
  return {
    colors,
    slides,
    pictures
  }
})
