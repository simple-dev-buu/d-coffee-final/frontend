export type CusHome = {
  name: string
  tel: string
  birthDay: Date
  image: string
  point: number
  registerDate: Date

}
