import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import { useMessageStore } from '@/stores/message'
import type { ReceiptItem } from '@/views/receipt/types/receiptItem'
import type { Receipt } from '@/views/receipt/types/receipt'
import receiptService from '@/services/receiptold'
import { generatePDF } from '@/utils/pdf'
import { useAuthStore } from '@/stores/auth'
import { useLoadingStore } from '@/stores/loading'

export const useReceiptStore = defineStore('receiptPos', () => {
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const receiptItems = ref<ReceiptItem[]>([])
  const receipt = ref<Receipt>()
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      totalUnit: 0,
      totalBefore: 0,
      discount: 0,
      netPrice: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      givePoint: 0,
      usePoint: 0,
      totalPoint: 0,
      createdDateTime: new Date(),
      empId: authStore.getCurrentUser()?.empId!,
      cusId: -1,
      branchId: authStore.getCurrentUser()?.branch.id!
    }
    receiptItems.value = []
  }
  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )
  const calReceipt = function () {
    receipt.value!.totalBefore = 0
    receipt.value!.totalUnit = 0
    receipt.value!.change = 0
    for (let i = 0; i < receiptItems.value.length; i++) {
      receipt.value!.totalBefore += receiptItems.value[i].price * receiptItems.value[i].unit
      receipt.value!.totalUnit += receiptItems.value[i].unit
      // receipt.value!.change = receiptItems.value[i].price - receiptItems.value[i].receivedAmount
      // console.log(receipt.value!.change)
    }
  }

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    receiptItems.value.push(newReceiptItem)
  }
  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      deleteReceiptItem(selectedItem)
    }
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
  }
  const order = async () => {
    try {
      console.log('Start Export')
      // exportPDF()
      console.log('Finish Export')
      loadingStore.doLoad()
      await receiptService.addReceipt(receipt.value!, receiptItems.value)
      initReceipt()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  return {
    receipt,
    receiptItems,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    removeItem,
    order,
  }
})
