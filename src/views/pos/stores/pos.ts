import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import ProductService from '@/services/product'
import type { Product } from '@/views/product/types/Product'
import { useMessageStore } from '@/stores/message'
import { ReceiptService } from '@/services/receipt'
import { defaultReceipt, type Receipt } from '@/views/receipt/types/receipt'
import type { ReceiptItem } from '@/views/receipt/types/receiptItem'
import { useAuthStore } from '@/stores/auth'
import type { Customer } from '@/views/customer/types/Customer'
import { CustomersService } from '@/services/customer'
import type { Promotion } from '@/views/promotion/types/Promotion'
import { PromotionService } from '@/services/promotion'

export const usePosStore = defineStore('pos', {
  state() {
    return {
      auth: useAuthStore(),
      cusService: new CustomersService(),
      pdService: new ProductService(),
      products1: ref<Product[]>([]),
      products2: ref<Product[]>([]),
      products3: ref<Product[]>([]),
      messageStore: useMessageStore(),
      tempReceipt: ref<Receipt>({ ...defaultReceipt }),
      availableCustomer: ref<Customer[]>([]),
      totalNet: ref<number>(0),
      readyToPay: ref<boolean>(false),
      dialogReceipt: ref<boolean>(false),
      selectedCustomer: ref<Customer>(),
      tempUsePoint: ref<number>(0),
      tempTotalPoint: ref<number>(0),
      promotionDialog: ref<boolean>(false),
      availablePromotion: ref<Promotion[]>([]),
      selectedPromotion: ref<Promotion>()
    }
  },
  actions: {
    async fetchPromotions() {
      this.availablePromotion = await PromotionService.getAll()
    },
    closeDialogPromotion() {
      this.promotionDialog = false
      this.selectedPromotion = undefined
    },
    openDialogPromotion() {
      this.promotionDialog = true
    },
    saveSelectPromotion() {
      this.tempReceipt.promotionId = this.selectedPromotion?.id
      this.tempReceipt.promotion = this.selectedPromotion
      this.promotionDialog = false
    },
    openDialogReceipt() {
      if (this.auth.getCurrentUser()) {
        this.tempReceipt.branch = { ...this.auth.getCurrentUser()?.branch!, location: '' }
        this.tempReceipt.branchId = this.auth.getCurrentUser()?.branch.id ?? 0
      }
      if (this.selectedCustomer) {
        this.tempReceipt.cusId = this.selectedCustomer.id!
        this.tempReceipt.customer = this.selectedCustomer
        this.tempReceipt.totalPoint = this.selectedCustomer.point
        this.calPoint()
      }
      this.dialogReceipt = true
    },
    closeDialogReceipt() {
      this.dialogReceipt = false
    },
    async fetchCustomerByTel(tel: string) {
      this.selectedCustomer = await this.cusService.getByTel(tel)
    },
    async fetchCustomers() {
      this.availableCustomer = await this.cusService.getAll()
    },
    async fetchProducts() {
      try {
        //   loadingStore.doLoad()
        this.products1 = await this.pdService.getProductByType(1)
        this.products2 = await this.pdService.getProductByType(2)
        this.products3 = await this.pdService.getProductByType(3)
        //   loadingStore.finish()
      } catch (e) {
        console.error(e)
        //   loadingStore.finish()
      }
    },
    async saveReceipt() {
      try {
        if (this.tempReceipt) {
          this.tempReceipt.empId = this.auth.getCurrentUser()?.empId ?? 0
          const res = await ReceiptService.create(this.tempReceipt)
          if (res) this.messageStore.showMessage('Success', 'success')
          nextTick(() => {
            this.resetTempReceipt()
          })
        }
      } catch (e: any) {
        this.messageStore.showMessage(e.message)
      }
    },
    resetTempReceipt() {
      this.tempReceipt = { ...defaultReceipt }
      this.tempReceipt.receiptItems = []
      this.calReceipt()
    },
    addReceiptItem(item: ReceiptItem) {
      if (this.tempReceipt) {
        const index = this.tempReceipt.receiptItems!.findIndex(
          (receiptItem) => receiptItem.name === item.name
        )
        if (index > -1) {
          this.tempReceipt.receiptItems![index].unit++
        } else {
          this.tempReceipt.receiptItems!.push(item)
        }
        this.calReceipt()
      }
    },
    deleteReceiptItem(selectedItem: ReceiptItem) {
      const items = this.tempReceipt.receiptItems
      if (items) {
        const index = items.findIndex((item) => item === selectedItem)
        items.splice(index, 1)
        this.calReceipt()
      } else {
        alert('No items')
      }
    },
    calReceipt() {
      this.tempReceipt.totalBefore = 0
      this.tempReceipt.totalUnit = 0
      this.tempReceipt.change = 0
      this.tempReceipt.netPrice = 0

      for (let i = 0; i < this.tempReceipt.receiptItems!.length; i++) {
        const calTotal =
          this.tempReceipt.receiptItems![i].price * this.tempReceipt.receiptItems![i].unit
        this.tempReceipt.totalBefore += calTotal
        this.tempReceipt.receiptItems![i].total = calTotal
        this.tempReceipt.totalUnit += this.tempReceipt.receiptItems![i].unit
      }
      this.tempReceipt.netPrice = this.tempReceipt.totalBefore - this.tempReceipt.discount * 100
    },
    incUnitOfReceiptItem(selectedItem: ReceiptItem) {
      selectedItem.unit++
      this.calReceipt()
    },
    decUnitOfReceiptItem(selectedItem: ReceiptItem) {
      selectedItem.unit--
      if (selectedItem.unit === 0) {
        this.deleteReceiptItem(selectedItem)
      }
      this.calReceipt()
    },
    removeItem(item: ReceiptItem) {
      const index = this.tempReceipt.receiptItems!.findIndex((ri) => ri === item)
      this.tempReceipt.receiptItems!.splice(index, 1)
      this.calReceipt()
    },
    calNetPrice() {
      this.tempReceipt.receiptItems!.forEach((item) => {
        this.tempReceipt.netPrice += item.price * item.unit
      })
    },
    calMoney(money: number) {
      if (money > 0) {
        this.tempReceipt.receivedAmount = money
        this.tempReceipt.change = money - this.tempReceipt.netPrice
      }
    },
    usePoints() {
      if (this.selectedCustomer)
        if (this.tempUsePoint <= this.selectedCustomer.point) {
          this.tempReceipt.usePoint = this.tempUsePoint
          this.selectedCustomer.point -= this.tempUsePoint
          this.tempReceipt.discount = this.tempUsePoint
          this.tempReceipt.netPrice = this.tempReceipt.totalBefore - this.tempReceipt.discount
          this.tempReceipt.totalPoint -= this.tempUsePoint
        }
    },
    calPoint() {
      if (this.tempReceipt.totalBefore >= 100) {
        const givePoint = Math.round(this.tempReceipt.totalBefore / 100)
        this.tempReceipt.givePoint = givePoint
        this.tempReceipt.totalPoint += givePoint
      }
    }
  }
})
