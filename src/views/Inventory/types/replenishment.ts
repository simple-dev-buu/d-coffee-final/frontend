import type { Ingredient } from '@/views/Ingredient/types/ingredient'
import type { Inventory } from './inventory'
import type { Employee } from '@/views/employee/types/Employee'

export type ReplenishmentItem = {
  id?: number
  ingredientId: number | undefined
  ingredient?: Ingredient
  quantity: number
  value: number
}

export const defaultReplenishmentItem: ReplenishmentItem = {
  ingredientId: undefined,
  quantity: 0,
  value: 0
}

export type Replenishment = {
  id?: number
  empId: number
  employee?: Employee
  inventoryId: number
  inventory?: Inventory
  createdDate?: string
  createdTime?: string
  totalCost: number
  replenishmentItems: ReplenishmentItem[]
  inventoryName?: string
  employeeFullName?: string
}

export const defaultReplenishment: Replenishment = {
  empId: 0,
  inventoryId: 0,
  createdDate: '',
  totalCost: 0,
  replenishmentItems: []
}
