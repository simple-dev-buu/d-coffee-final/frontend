import type { Inventory } from '../../Inventory/types/inventory'

export const defaultBranch: Branch = {
  name: '',
  location: ''
}
export interface Branch {
  id?: number
  name: string
  location: string
  createdDate?: string
  inventory?: Inventory
}
