export type UnitTypes = 'กิโลกรัม' | 'ลิตร'

export const defaultUnitTypes: UnitTypes[] = ['กิโลกรัม', 'ลิตร']

export type Ingredient = {
  id?: number
  name: string
  unit: UnitTypes
}

export const defaultIngredient: Ingredient = {
  name: '',
  unit: 'กิโลกรัม'
}
