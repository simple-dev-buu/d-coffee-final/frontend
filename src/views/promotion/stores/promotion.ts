import { defineStore } from 'pinia'
import { ref, watch } from 'vue'
import type { Promotion } from '../types/Promotion'
import { useProductStore } from '@/views/product/stores/product'
import { PromotionService } from '@/services/promotion'
import type { Product } from '@/views/product/types/Product'
export const usePromotionStore = defineStore('promotion', () => {
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Photo', key: 'image', sortable: false },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Description', key: 'description', sortable: false },
    { title: 'Combo', key: 'comboSet', sortable: false },
    { title: 'Discount', key: 'discount', sortable: false },
    { title: 'Start Date', key: 'actions', sortable: false },
    { title: 'End Date', key: 'actions', sortable: false },
    { title: 'Status', key: 'actions', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false }
  ]
  const items = ref<Promotion[]>([])
  const productStore = useProductStore()
  const promotionService = new PromotionService()
  const initialPromotion: Promotion & { files: File[] } = {
    name: '',
    description: '',
    comboSet: false,
    discount: 0,
    startDate: '',
    endDate: '',
    status: false,
    usableProducts: [],
    files: []
  }
  const promotionItem = ref<Promotion>(JSON.parse(JSON.stringify(initialPromotion)))
  const selectedProduct = ref<Product[]>([])

  watch(
    () => selectedProduct.value,
    () => {
      extractId()
    },
    { deep: true }
  )
  const idArray = ref<(number | undefined)[]>([])
  // const promotionItems = reactive<Promotion>({
  //   name: '',
  //   description: '',
  //   discount: 0,
  //   comboSet: false,
  //   image: 'noimage.jpg',
  //   startDate: '',
  //   endDate: '',
  //   status: false,
  //   usableProduct: [0]
  // })
  const dialog = ref(false)

  const openDialog = () => {
    dialog.value = true
    productStore.getAll()
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }

  const extractId = () => {
    promotionItem.value.usableProducts = selectedProduct.value.map((item): number =>
      item.id ? item.id : 0
    )
  }

  const save = async () => {
    try {
      // loadingStore.doLoad()
      const promotion = promotionItem.value
      if (!promotion.id) {
        // Add new
        console.log('Post ' + JSON.stringify(promotion))
        const res = await promotionService.save(promotion)
      } else {
        // Update
        // console.log('Patch ' + JSON.stringify(promotion))
        const res = await promotionService.update(promotion)
      }
      await promotionService.getAll()
      closeDialog()
      // loadingStore.finish()
    } catch (e: any) {
      // messageStore.showMessage(e.message)
      // loadingStore.finish()
    }
  }
  const resetItem = () => {
    promotionItem.value = JSON.parse(JSON.stringify(initialPromotion))
  }
  const editItem = async (item: Promotion) => {
    // promotionItem.id = item.id
    // promotionItem.name = item.name
    // promotionItem.description = item.description
    // promotionItem.comboSet = item.comboSet
    // promotionItem.discount = item.discount
    // promotionItem.startDate = item.startDate
    // promotionItem.endDate = item.endDate
    // promotionItem.status = item.status
    // console.log(promotionItem.status)

    openDialog()
    if (item.id) {
      await promotionService.getPromotion(item.id)
      openDialog()
    }
  }
  const deleteItem = async (item: Promotion) => {
    if (item.id) {
      await promotionService.delete(item.id)
      getAll()
    }
  }

  const getAll = async () => {
    items.value = await promotionService.getAll()
  }

  return {
    getAll,
    headers,
    items,
    dialog,
    promotionItem,
    selectedProduct,
    idArray,
    openDialog,
    editItem,
    deleteItem,
    closeDialog,
    save
  }
})
