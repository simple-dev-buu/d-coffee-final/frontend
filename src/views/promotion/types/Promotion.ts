type Promotion = {
  id?: number
  name: string
  description: string
  discount: number
  comboSet: boolean
  image?: string
  startDate: string
  endDate: string
  status: boolean
  usableProducts: number[]
  files?: File[]
}

export type { Promotion }
