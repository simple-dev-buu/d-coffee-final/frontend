import { defineStore } from 'pinia'

export const usepicEmStore = defineStore('pictureEm', () => {
  const colors = [
    'indigo',
    'warning',
    'pink darken-2',
    'red lighten-1',
    'deep-purple accent-4',
    'blue'
  ]
  const slides = ['src/views/EmployeeHome/image/New.png', 'src/views/EmployeeHome/image/New1.png']

  return {
    colors,
    slides
  }
})
