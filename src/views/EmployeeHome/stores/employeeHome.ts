import { useAuthStore } from '@/stores/auth'
import type { User } from '@/views/user/types/User'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import EmployeeService from '@/services/employee'
import attendance from '@/services/attendance'
import type { EmployeeHome } from '../types/employeeHome'

export const useEmployeeHomeStore = defineStore('profileEm', () => {
  const authStore = useAuthStore()
  const initEmployee: EmployeeHome & { files: File[] } = {
    moneyRate: 0,
    minwork: 0,
    startDate: new Date(),
    endDate: null,
    timeWorked: 0,
    clockin: '',
    clockout: '',
    files: []
  }
  const employeeData = ref<EmployeeHome & { files: File[] }>(JSON.parse(JSON.stringify(initEmployee)))

  function openEmHome() {
    getEmHome()
    getEmHomeTime()
  }
  async function getEmHome() {
    const getUser = authStore.getCurrentUser()
    if (getUser?.empId) {
      const respond = await EmployeeService.getEmployee(getUser.empId)
      const user = respond.data
      employeeData.value.moneyRate = user.moneyRate
      employeeData.value.minwork = user.minWork
      employeeData.value.startDate = user.startDate
      employeeData.value.endDate = user.endDate

    }
  }

  async function getEmHomeTime() {
    const getUser = authStore.getCurrentUser()
    if (getUser?.empId) {
      const respond = await attendance.getAttendance(getUser.empId)
      const user = respond.data
      employeeData.value.timeWorked = user.timeWorked
      employeeData.value.clockin = user.clockin
      employeeData.value.clockout = user.clockout
    }
  }

  
  return {
    employeeData,
    openEmHome,
    getEmHome,
    getEmHomeTime
  }
})
