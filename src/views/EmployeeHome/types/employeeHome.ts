export type EmployeeHome = {
  moneyRate: number
  minwork: number
  startDate: Date
  endDate: Date | null
  timeWorked: number
  clockin: string
  clockout: string | null
  

}
