import { defineStore } from 'pinia'
import { reactive, ref } from 'vue'
import { defaultBill, type Bill } from '../types/Bill'
import { BillsService } from '@/services/bill'
export const useBillStore = defineStore('bill', {
  state() {
    return {
      filterQuery: reactive({ month: 0, year: 0 }),
      isDialogOpen: ref(false),
      Dialogcheck: ref(false),
      billList: ref<Bill[]>([]),
      tempItem: ref<Bill>({ ...defaultBill }),
      headers: [
        { title: 'ID', key: 'id' },
        { title: 'Branch', key: 'branch.name' },
        { title: 'Name', key: 'name', sortable: false },
        { title: 'CreateDate', key: 'createdDate', sortable: false },
        { title: 'Worth', key: 'worth', sortable: false }
      ]
    }
  },
  getters: {
    getTitleDialog: (state) => {
      if (state.tempItem.id) {
        return 'Bill Add'
      }
    }
  },
  actions: {
    async fetchFilterBill() {
      if (this.filterQuery.month > 0 || this.filterQuery.year > 0) {
        this.billList = await BillsService.getAllWithFilter(
          this.filterQuery.year,
          this.filterQuery.month
        )
      } else {
        alert('Please select month and year valid !')
      }
    },
    async fetchAll() {
      this.billList = await BillsService.getAll()
    },
    openDialog(item?: Bill) {
      if (item) {
        this.tempItem = item
      }
      this.isDialogOpen = true
    },
    openDialogCheck() {
      this.Dialogcheck = true
    },
    closeDialog() {
      this.resetItem()
      this.isDialogOpen = false
    },
    closeDialogCheck() {
      this.Dialogcheck = false
    },
    resetItem() {
      this.tempItem = { ...defaultBill }
    },
    async deleteItem(id: number) {
      await BillsService.delete(id)
      this.resetItem()
    },
    async save() {
      if (this.tempItem.id) {
        await BillsService.update(this.tempItem)
      } else {
        await BillsService.create(this.tempItem)
      }
      this.fetchAll()
    },
    async getElecOrWater(
      billType: string,
      targetMonth: number,
      specificBranch: number,
      targetYear: number
    ) {
      const res = await BillsService.findBillThisMonth(
        billType,
        targetMonth,
        specificBranch,
        targetYear
      )
      console.log('Query Successful')
      this.closeDialogCheck()
      alert(
        'จำนวนบิลล์' +
          billType +
          'ของเดือน ' +
          targetMonth +
          ' ของปี ' +
          targetMonth +
          ' ของสาขาที่ ' +
          specificBranch +
          ' คือ ' +
          Object.values(res) +
          ' บาท'
      )
    },
    async getSumBill(billType: string, targetMonth: number, targetYear: number) {
      if (targetMonth > 0 || targetYear > 0 || billType != '') {
        const res = await BillsService.getSumWithFilter(billType, targetYear, targetMonth)
        console.log(res)
        this.closeDialogCheck()
        alert(
          'จำนวนบิลล์ ' +
            billType +
            ' ของเดือน ' +
            targetMonth +
            ' ของปี ' +
            targetMonth +
            ' คือ ' +
            Object.values(res) +
            ' บาท'
        )
      } else {
        alert('Please select')
      }
    }
  }
})
