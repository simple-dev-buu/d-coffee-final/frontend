export type Bill = {
  id?: number
  name: string
  createdDate: Date
  worth: number
}
export const defaultBill = <Bill>{
  name: '',
  createdDate: new Date(),
  worth: 0
}
