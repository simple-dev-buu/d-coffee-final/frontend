import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { PayrollDetail } from '../types/PayrollDetail'
import { usePayrollStore } from './payroll'
import type { Payroll } from '../types/Payroll'

export const usePayrollDetailStore = defineStore('payrollDetail', () => {
  const payrollStore = usePayrollStore()
  const dialog = ref(false)

  const item = ref<Payroll>({} as Payroll)
  const itemEdit = ref<Payroll>({} as Payroll)
  const itemDetail = ref<PayrollDetail>({} as PayrollDetail)

  const dialogDetailView = ref(false)

  const initPayroll: Payroll = {
    employee: {} as Payroll['employee'],
    totalNet: 0.0,
    totalDeduction: 0.0,
    payrollStatus: 'Pending',
    payrollDetail: {
      salary: 0.0,
      overtime: 0.0,
      commission: 0.0,
      allowances: 0.0,
      bonus: 0.0,
      otherEarning: 0.0,
      socialSecurityFund: 0.0,
      withHoldingTax: 0.0,
      studentLoanFund: 0.0,
      deposit: 0.0,
      leave: 0.0,
      otherDeduction: 0.0,
      ytdEarning: 0.0,
      ytdWithHoldingTax: 0.0,
      collectSSF: 0.0,
      totalEarning: 0.0,
      totalDeduction: 0.0,
      netPay: 0.0
    }
  }

  const openDialog = (editItem: Payroll) => {
    itemEdit.value = editItem
    dialog.value = true
  }
  const closeDialog = () => {
    dialog.value = false
    itemEdit.value = JSON.parse(JSON.stringify(initPayroll))
  }

  function openDialogDetailView(itemView: Payroll) {
    console.log(itemView)
    item.value = itemView
    itemDetail.value = itemView.payrollDetail
    dialogDetailView.value = true
  }

  function closeDialogDetailView() {
    dialogDetailView.value = false
  }

  function savePayrollDetail() {
    calculatePayrollDetail()
    const deleteIndex = payrollStore.itemsPending.findIndex((item) => item.employee?.id === itemEdit.value.employee?.id)
    payrollStore.itemsPending.splice(deleteIndex, 1)
    payrollStore.itemsPending.push(itemEdit.value)
    console.log(JSON.stringify(item.value))
    closeDialog()
  }

  function calculatePayrollDetail() {
    let totalEarning = 0
    let totalDeduction = 0

    totalEarning =
      Number(itemEdit.value.payrollDetail.salary) +
      Number(itemEdit.value.payrollDetail.overtime) +
      Number(itemEdit.value.payrollDetail.commission) +
      Number(itemEdit.value.payrollDetail.allowances) +
      Number(itemEdit.value.payrollDetail.bonus) +
      Number(itemEdit.value.payrollDetail.otherEarning)

    itemEdit.value.payrollDetail.totalEarning = totalEarning

    console.log('totalEarning ' + totalEarning)

    totalDeduction =
    Number(itemEdit.value.payrollDetail.socialSecurityFund) +
    Number(itemEdit.value.payrollDetail.withHoldingTax) +
    Number(itemEdit.value.payrollDetail.studentLoanFund) +
    Number(itemEdit.value.payrollDetail.deposit) +
    Number(itemEdit.value.payrollDetail.leave) +
    Number(itemEdit.value.payrollDetail.otherDeduction)

    itemEdit.value.payrollDetail.totalDeduction = totalDeduction

    console.log('totalDeduction ' + totalDeduction)

    itemEdit.value.payrollDetail.netPay =
      Number(itemEdit.value.payrollDetail.totalEarning) - Number(itemEdit.value.payrollDetail.totalDeduction)

    itemEdit.value.totalNet = itemEdit.value.payrollDetail.netPay
    itemEdit.value.totalDeduction = itemEdit.value.payrollDetail.totalDeduction

    console.log('totalNet ' + itemEdit.value.totalNet)

    console.log(itemEdit.value)

    return itemEdit
  }

  return {
    item,
    itemEdit,
    itemDetail,
    dialog,
    openDialog,
    closeDialog,
    savePayrollDetail,
    calculatePayrollDetail,
    dialogDetailView,
    openDialogDetailView,
    closeDialogDetailView
  }
})
