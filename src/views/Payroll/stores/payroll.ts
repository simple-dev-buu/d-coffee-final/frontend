import { defineStore } from 'pinia'
import { ref } from 'vue'

import type { Payroll } from '../types/Payroll'
import PayrollService from '@/services/payroll'
import { usePayrollDetailStore } from './payrollDetail'
import { useLoadingStore } from '@/stores/loading'
import type { Employee } from '@/views/employee/types/Employee'
import { generatePDF } from '@/utils/pdf'

export const usePayrollStore = defineStore('payroll', () => {
  // const filterQuery = reactive({ month: 0, year: 0 })
  const loadingStore = useLoadingStore()
  const payrollDetailStore = usePayrollDetailStore()
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Date', key: 'createdDate' },
    { title: 'Employee', key: `employee.id`, sortable: false },
    { title: 'Total Net', key: 'totalNet' },
    { title: 'Total Deduction', key: 'totalDeduction' }
  ]
  const headersPending = [
    { title: 'Employee ID', key: 'id' },
    { title: 'Name', key: `firstName + ' ' + lastName`, sortable: false },
    { title: 'Total Net', key: 'totalNet' }
  ]

  const items = ref<Payroll[]>([])
  const itemDetailEdit = ref<Payroll>()
  const itemsPending = ref<Payroll[]>([])
  const initialPayroll: Payroll = {
    employee: {} as Employee,
    totalNet: 0,
    totalDeduction: 0,
    payrollStatus: 'Pending',
    payrollDetail: {
      salary: 0,
      overtime: 0,
      commission: 0,
      allowances: 0,
      bonus: 0,
      otherEarning: 0,
      socialSecurityFund: 0,
      withHoldingTax: 0,
      studentLoanFund: 0,
      deposit: 0,
      leave: 0,
      otherDeduction: 0,
      ytdEarning: 0,
      ytdWithHoldingTax: 0,
      collectSSF: 0,
      totalEarning: 0,
      totalDeduction: 0,
      netPay: 0
    }
  }

  const payrollItem = ref<Payroll>(JSON.parse(JSON.stringify(initialPayroll)))
  const dialog = ref(false)

  const openDialog = async () => {
    await getPendingEmployee()
    dialog.value = true
  }
  const closeDialog = () => {
    dialog.value = false
    resetItem()
  }

  async function save() {
    try {
      loadingStore.doLoad()
      await PayrollService.savePayroll(itemsPending.value)
      closeDialog()
      loadingStore.finish()
    } catch (e: any) {
      console.log('frontend payroll error: ' + e.message)
    }
    reloadPayroll()
  }

  const resetItem = () => {
    itemsPending.value = []
  }

  const editItem = async () => {
    openDialog()
  }

  const deleteItem = async (item: Payroll) => {
    await PayrollService.deletePayroll(item)
    reloadPayroll()
  }

  const editPendingItem = async (item: Payroll) => {
    payrollDetailStore.openDialog(item)
  }

  const deletePendingItem = async (item: Payroll) => {
    itemsPending.value = itemsPending.value.filter((i) => i.employee?.id !== item.employee?.id)
  }

  function getMonthYear() {
    const date = new Date()
    const monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ]
    const month = monthNames[date.getMonth()]
    const year = date.getFullYear()
    return month + ' ' + year
  }
  function getDateString() {
    const date = new Date()
    return formatDate(date)
  }

  async function getPendingEmployee() {
    const respond = await PayrollService.getAllEmployee()
    for (const itemEmp of respond.data) {
      const itemPending = ref<Payroll>(JSON.parse(JSON.stringify(initialPayroll)))
      itemPending.value.employee = itemEmp
      itemPending.value.payrollDetail.salary = parseFloat(itemEmp.moneyRate)
      calculatePayrollDetail(itemPending.value)
      itemsPending.value.push(itemPending.value)
    }
  }

  function getTotalPendingPayroll() {
    let total = 0
    itemsPending.value.forEach((item) => {
      total += item.totalNet
    })
    return total
  }

  async function getPayrolls() {
    const respond = await PayrollService.getPayrolls()
    items.value = respond.data
  }

  function getEmpCount() {
    let count = 0
    itemsPending.value.forEach(() => {
      count += 1
    })
    return count
  }

  function formatDate(date: Date) {
    const d = new Date(date)
    let month = '' + (d.getMonth() + 1)
    let day = '' + d.getDate()
    const year = d.getFullYear()
    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day
    return [year, month, day].join('-')
  }

  function calculatePayrollDetail(item: Payroll) {
    let totalEarning = 0
    let totalDeduction = 0

    totalEarning =
      Number(item.payrollDetail.salary) +
      Number(item.payrollDetail.overtime) +
      Number(item.payrollDetail.commission) +
      Number(item.payrollDetail.allowances) +
      Number(item.payrollDetail.bonus) +
      Number(item.payrollDetail.otherEarning)

    item.payrollDetail.totalEarning = totalEarning

    totalDeduction =
      Number(item.payrollDetail.socialSecurityFund) +
      Number(item.payrollDetail.withHoldingTax) +
      Number(item.payrollDetail.studentLoanFund) +
      Number(item.payrollDetail.deposit) +
      Number(item.payrollDetail.leave) +
      Number(item.payrollDetail.otherDeduction)

    item.payrollDetail.totalDeduction = totalDeduction

    item.payrollDetail.netPay =
      Number(item.payrollDetail.totalEarning) - Number(item.payrollDetail.totalDeduction)

    item.totalNet = item.payrollDetail.netPay
    item.totalDeduction = item.payrollDetail.totalDeduction

    return item
  }

  function reloadPayroll() {
    getPayrolls()
  }

  function downloadPDF() {
    const headers = [
      'ID',
      'Employee ID',
      'Employee Name',
      'Date',
      'Earning',
      'Deduction',
      'Net Pay'
    ]
    const data = items.value.map((item) => [
      item.id as unknown as string,
      item.employee?.id as unknown as string,
      item.employee?.firstName + ' ' + item.employee?.lastName,
      String(item.createdDate) ?? '-',
      String(item.payrollDetail.totalEarning) ?? '-',
      String(item.payrollDetail.totalDeduction) ?? '-',
      String(item.payrollDetail.netPay) ?? '-'
    ])
    const pdfFor = 'payroll'
    const otherDetail = {
      createdDate: new Date().toDateString()
    }
    console.log(data)
    generatePDF('Payroll History', data, headers, 'payroll-history', pdfFor, otherDetail)
  }

  function downloadOnePDF(item: Payroll) {
    const headers = [
      'ID',
      'Employee ID',
      'Employee Name',
      'Date',
      'Earning',
      'Deduction',
      'Net Pay'
    ]
    const data = [
      [
        item.id as unknown as string,
        item.employee?.id as unknown as string,
        item.employee?.firstName + ' ' + item.employee?.lastName,
        String(item.createdDate) ?? '-',
        String(item.payrollDetail.totalEarning) ?? '-',
        String(item.payrollDetail.totalDeduction) ?? '-',
        String(item.payrollDetail.netPay) ?? '-'
      ]
    ]
    const pdfFor = 'payroll'
    const otherDetail = {
      createdDate: new Date().toDateString()
    }
    console.log(data)
    generatePDF('Payroll History', data, headers, 'payroll-history', pdfFor, otherDetail)
  }

  return {
    // filterQuery,
    headers,
    headersPending,
    items,
    itemsPending,
    itemDetailEdit,
    dialog,
    payrollItem,
    openDialog,
    editItem,
    deleteItem,
    editPendingItem,
    deletePendingItem,
    closeDialog,
    save,
    getMonthYear,
    getDateString,
    getPendingEmployee,
    getTotalPendingPayroll,
    getPayrolls,
    getEmpCount,
    formatDate,
    reloadPayroll,
    downloadPDF,
    downloadOnePDF
  }
})
