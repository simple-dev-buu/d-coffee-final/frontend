export type PayrollDetail = {
  id?: number
  salary: number
  overtime: number
  commission: number
  allowances: number
  bonus: number
  otherEarning: number
  socialSecurityFund: number
  withHoldingTax: number
  studentLoanFund: number
  deposit: number
  leave: number
  otherDeduction: number
  ytdEarning: number
  ytdWithHoldingTax: number
  collectSSF: number
  totalEarning: number
  totalDeduction: number
  netPay: number
}
