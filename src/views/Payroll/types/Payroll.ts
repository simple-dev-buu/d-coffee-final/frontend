import type { Employee } from '@/views/employee/types/Employee'
import type { PayrollDetail } from './PayrollDetail'

export type Payroll = {
  id?: number
  createdDate?: Date
  employee: Employee
  totalNet: number
  totalDeduction: number
  payrollStatus: string
  payrollDetail: PayrollDetail
}
