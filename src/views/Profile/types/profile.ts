export type Profile = {
  profileId?: number
  name: string
  email: string
  tel: string
  image: string
  birthDay: string
  role: string
  firstName: string
  lastName: string
  point?: number
  registerDate?: string
}
