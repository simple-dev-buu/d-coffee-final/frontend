import { useAuthStore } from '@/stores/auth'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import EmployeeService from '@/services/employee'
import { CustomersService } from '@/services/customer'
import type { Profile } from '../types/profile'
import type { Employee } from '@/views/employee/types/Employee'
import type { Customer } from '@/views/customer/types/Customer'
import { useLoadingStore } from '@/stores/loading'

export const useProfileStore = defineStore('profile', () => {
  const loadingStore = useLoadingStore()
  const customersService = new CustomersService()
  const authStore = useAuthStore()
  const initProfile: Profile = {
    name: '',
    email: '',
    birthDay: '',
    tel: '',
    image: 'noimage.jpg',
    role: '',
    firstName: '',
    lastName: '',
    registerDate: ''
  }
  const profile = ref<Profile>(JSON.parse(JSON.stringify(initProfile)))
  const editEmployee = ref<Employee & { files: File[] }>({} as Employee & { files: File[] })
  const editCustomer = ref<Customer & { files: File[] }>({} as Customer & { files: File[] })

  function openProfile() {
    getProfile()
  }
  async function getProfile() {
    loadingStore.doLoad()
    const getUser = authStore.getCurrentUser()
    console.log(getUser)
    if (getUser?.empId) {
      const respond = await EmployeeService.getEmployee(getUser.empId)
      const user = respond.data
      profile.value.name = user.firstName + ' ' + user.lastName
      profile.value.email = getUser.email
      profile.value.birthDay = formatDate(user.birthDay)
      profile.value.registerDate = formatDate(user.startDate)
      profile.value.tel = user.tel
      profile.value.image = user.image
      profile.value.role = 'Employee'
      profile.value.firstName = user.firstName
      profile.value.lastName = user.lastName

      editEmployee.value = JSON.parse(JSON.stringify(respond.data))
    } else if (getUser?.cusId) {
      const user = await customersService.getCustomer(getUser.cusId)
      profile.value.name = user.name
      profile.value.email = getUser.email
      profile.value.birthDay = formatDate(user.birthDay) || ''
      profile.value.tel = user.tel
      profile.value.image = user.image
      profile.value.role = 'Customer'
      profile.value.point = user.point
      profile.value.registerDate = formatDate(user.registerDate)
      profile.value.firstName = user.name.split(' ')[0]
      profile.value.lastName = user.name.split(' ')[1]

      editCustomer.value = JSON.parse(JSON.stringify(user))
    }
    loadingStore.finish()
    console.log(profile.value)
  }

  function formatDate(date: Date) {
    const d = new Date(date)
    let month = '' + (d.getMonth() + 1)
    let day = '' + d.getDate()
    const year = d.getFullYear()
    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day
    return [year, month, day].join('-')
  }

  async function save() {
    if (profile.value.role === 'Employee') {
      await EmployeeService.update(editEmployee.value)
    }
    if (profile.value.role === 'Customer') {
      await customersService.update(editCustomer.value)
    }
    console.log(profile.value)
  }

  return {
    profile,
    openProfile,
    getProfile,
    save,
    editEmployee,
    editCustomer,
    formatDate
  }
})
