import type { Branch } from '@/views/Branch/types/branch'

export type Employee = {
  id?: number
  firstName: string
  lastName: string
  tel: string
  gender: 'Male' | 'Female' | 'Other'
  birthDay: Date | null
  title: string
  qualification: string
  moneyRate: number
  minWork: number
  startDate: Date
  endDate: Date | null

  fullName?: string
  image: string
}

function getImageUrl(employee: Employee) {
  return `http://localhost:3000/images/employees/${employee.image}`
}

export type { getImageUrl }
