import { defineStore } from 'pinia'
import { computed, ref } from 'vue'

import type { Employee } from '../types/Employee'
import type { VForm } from 'vuetify/components'
import EmployeeService from '@/services/employee'

export const useEmployeeStore = defineStore('employee', () => {
  // const employeeService = new EmployeeService()
  const headers = [
    { title: 'ID', key: 'id', sortable: true },
    { title: 'Image', key: 'image', sortable: false },
    { title: 'FirstName', key: 'firstName', sortable: false },
    { title: 'LastName', key: 'lastName', sortable: false },
    { title: 'Tel', key: 'tel', sortable: false },
    { title: 'Gender', key: 'gender', sortable: false },
    { title: 'BirthDay', key: 'birthDay', sortable: false },
    { title: 'Title', key: 'title', sortable: false },
    { title: 'Qualification', key: 'qualification', sortable: false },
    { title: 'MoneyRate', key: 'moneyRate', sortable: false },
    { title: 'MinWork', key: 'minWork', sortable: false },
    { title: 'StartDate', key: 'startDate', sortable: false },
    { title: 'EndDate', key: 'endDate', sortable: false },
    { title: 'Branch ID', key: 'branch.id', sortable: true }
  ]

  // ใช้หน้าEmployeeHome
  const headerHome = [
    { title: 'ID', key: 'id', sortable: true },
    { title: 'Image', key: 'image', sortable: false },
    { title: 'FirstName', key: 'firstName', sortable: false },
    { title: 'LastName', key: 'lastName', sortable: false },
    { title: 'Tel', key: 'tel', sortable: false },
    { title: 'Gender', key: 'gender', sortable: false },
    { title: 'BirthDay', key: 'birthDay', sortable: false },
    { title: 'Title', key: 'title', sortable: false },
    { title: 'Qualification', key: 'qualification', sortable: false },
    { title: 'Branch ID', key: 'branch.id', sortable: true }
  ]
  const headerHomeSalary = [
    { title: 'ID', key: 'id', sortable: true },
    { title: 'FirstName', key: 'firstName', sortable: false },
    { title: 'LastName', key: 'lastName', sortable: false },
    { title: 'MoneyRate', key: 'moneyRate', sortable: false },
    { title: 'MinWork', key: 'minWork', sortable: false },
    { title: 'StartDate', key: 'startDate', sortable: false },
    { title: 'EndDate', key: 'endDate', sortable: false },
  ]



  const items = ref<Employee[]>([])
  const initialEmployee: Employee & { files: File[] } = {
    firstName: '',
    lastName: '',
    tel: '',
    gender: 'Male',
    birthDay: null,
    title: '',
    qualification: '',
    moneyRate: 0,
    minWork: 0,
    startDate: new Date(),
    endDate: null,
    image: 'noimage.jpg',

    files: []
  }
  const employeeItem = ref<Employee & { files: File[] }>(
    JSON.parse(JSON.stringify(initialEmployee))
  )
  const dialog = ref(false)
  const checkDialog = ref(false)

  const openDialog = () => {
    dialog.value = true
  }
  const openDialogCheck = () => {
    checkDialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }

  const closeDialogCheck = () => {
    checkDialog.value = false
  }

  async function getAttendence(month: number, empId: number, year: number) {
    const respond = await EmployeeService.getAttendence(month, empId, year)
    alert(
      'จำนวนที่เข้างานของรหัสพนักงาน ' +
        empId +
        ' ในเดือน ' +
        month +
        ' ปี ' +
        year +
        ' คือ ' +
        Object.values(respond[0][0]) +
        ' ครั้ง'
    )
  }

  const refForm = ref<VForm | null>(null)

  async function getEmployee(id: number) {
    try {
      // loadingStore.doLoad()
      const res = await EmployeeService.getEmployee(id)
      employeeItem.value = res.data
      // loadingStore.finish()
    } catch (e: any) {
      // loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }

  const getAll = async () => {
    items.value = await EmployeeService.getAll()
  }

  async function save() {
    try {
      // loadingStore.doLoad()
      const employee = employeeItem.value
      if (!employee.id) {
        // Add new
        console.log('Post ' + JSON.stringify(employee))
        const res = await EmployeeService.update(employee)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(employee))
        const res = await EmployeeService.save(employee)
      }
      await getAll()
      closeDialog()
      // loadingStore.finish()
    } catch (e: any) {
      // messageStore.showMessage(e.message)
      // loadingStore.finish()
    }
  }

  const resetItem = () => {
    employeeItem.value = JSON.parse(JSON.stringify(initialEmployee))
  }

  const editItem = async (item: Employee) => {
    // employeeItem.id = item.id
    // employeeItem.name = item.name
    // employeeItem.price = item.price
    // employeeItem.category = item.category
    // console.log(employeeItem.category)
    openDialog()

    if (item.id) {
      await EmployeeService.getEmployee(item.id)
      openDialog()
      console.log(getEmployee(item.id))
    }
  }

  const deleteItem = async (item: Employee) => {
    if (item.id) {
      await EmployeeService.delete(item.id)
      getAll()
    }
  }

  return {
    checkDialog,
    headers,
    items,
    employeeItem,
    dialog,
    headerHome,
    headerHomeSalary,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    openDialogCheck,
    getAttendence,
    closeDialogCheck,
    save,
    resetItem,
    getAll
  }
})
