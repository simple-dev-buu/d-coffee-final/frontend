import { defineStore } from 'pinia'
import { ref } from 'vue'
import { defaultUser, type User } from '../types/User'
import { UserService } from '@/services/user'
import type { Employee } from '@/views/employee/types/Employee'
import type { Customer } from '@/views/customer/types/Customer'
import { CustomersService } from '@/services/customer'
import EmployeeService from '@/services/employee'
import type { Role } from '@/views/role/types/Role'
import { RolesServices } from '@/services/roles'

export const useUserStore = defineStore('user', {
  state() {
    return {
      headers: [
        { title: 'ID', key: 'id' },
        { title: 'Email', key: 'email', sortable: false },
        { title: 'Role', key: 'role.name', sortable: false }
      ],
      availableEmployees: ref<Employee[]>([]),
      availableCustomers: ref<Customer[]>([]),
      availableRoles: ref<Role[]>([]),
      users: ref<User[]>([]),
      userItem: ref<User>(defaultUser),
      formDialog: ref<boolean>(false)
    }
  },
  getters: {
    getEmployees: (s) => s.availableEmployees,
    getCustomer: (s) => s.availableCustomers,
    getRoles: (s) => s.availableRoles
  },
  actions: {
    async fetchUsers() {
      this.users = await UserService.getAll()
    },
    async fetchAvailableSelect() {
      this.availableCustomers = await new CustomersService().getAll()
      this.availableEmployees = await EmployeeService.getAll()
      this.availableRoles = await RolesServices.getAll()
    },
    save() {
      if (this.userItem) UserService.create(this.userItem)
      this.resetItem()
      this.closeDialog()
    },
    resetItem() {
      this.userItem = { ...defaultUser }
    },
    openDialog(item?: User) {
      if (item) this.userItem = { ...item }
      this.formDialog = true
    },
    closeDialog() {
      this.formDialog = false
      this.fetchUsers()
    },
    async deleteItem(id: number) {
      UserService.delete(id)
      this.fetchUsers()
    }
  }
})
