import type { Role } from '@/views/role/types/Role'

export type User = {
  id?: number
  email: string
  password?: string
  empId?: number
  cusId?: number
  role?: Role
  roleName: string
}

export const defaultUser: User = {
  email: '',
  roleName: ''
}
