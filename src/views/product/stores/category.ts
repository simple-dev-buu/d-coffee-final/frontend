// import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import categoryService from '@/services/category'
import type { Category } from '@/views/product/types/Category'

export const useCategoryStore = defineStore('category', () => {
  //   const loadingStore = useLoadingStore()
  const categories = ref<Category[]>([])
  const initialCategory: Category = {
    name: ''
  }
  const editedCategory = ref<Category>(JSON.parse(JSON.stringify(initialCategory)))

  async function getCategory(id: number) {
    // loadingStore.doLoad()
    const res = await categoryService.getCategory(id)
    editedCategory.value = res.data
    // loadingStore.finish()
  }
  async function getCategories() {
    try {
      //   loadingStore.doLoad()
      const res = await categoryService.getCategories()
      categories.value = res.data
      console.log(res)
      console.log(res.data)
      console.log(categories.value)
      console.log(categories)
      //   loadingStore.finish()
    } catch (e) {
      //   loadingStore.finish()
    }
  }
  async function saveCategory() {
    // loadingStore.doLoad()
    const category = editedCategory.value
    if (!category.id) {
      // Add new
      console.log('Post ' + JSON.stringify(category))
      const res = await categoryService.addCategory(category)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(category))
      const res = await categoryService.updateCategory(category)
    }

    await getCategories()
    // loadingStore.finish()
  }
  async function deleteCategory() {
    // loadingStore.doLoad()
    const category = editedCategory.value
    const res = await categoryService.delCategory(category)

    await getCategories()
    // loadingStore.finish()
  }

  function clearForm() {
    editedCategory.value = JSON.parse(JSON.stringify(initialCategory))
  }
  return {
    categories,
    getCategories,
    saveCategory,
    deleteCategory,
    editedCategory,
    getCategory,
    clearForm
  }
})
