import { defineStore } from 'pinia'
import { ref } from 'vue'

import type { Product } from '../types/Product'
import ProductService from '@/services/product'
import { useLoadingStore } from '@/stores/loading'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const productService = new ProductService()
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Photo', key: 'image', sortable: false },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Price', key: 'price', sortable: false },
    {
      title: 'Category',
      key: 'category',
      values: (item: any | Product) => item.category.name,
      sortable: false
    },
    { title: 'Action', key: 'action', sortable: false }
  ]
  const items = ref<Product[]>([])
  const drink = ref<Product[]>([])
  const bakery = ref<Product[]>([])
  const food = ref<Product[]>([])
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0,
    category: { id: 1, name: 'Drink' },
    image: 'noimage.jpg',
    files: []
  }
  const productItem = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))
  const dialog = ref(false)

  const openDialog = () => {
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }

  const getAll = async () => {
    loadingStore.doLoad()
    items.value = await productService.getAll()
    loadingStore.finish()
  }

  const getCategoryProducts = async () => {
    drink.value = await productService.getProductByType(1)
    bakery.value = await productService.getProductByType(2)
    food.value = await productService.getProductByType(3)
  }

  async function getProduct(id: number) {
    try {
      loadingStore.doLoad()
      const res = await productService.getProduct(id)
      productItem.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }

  async function save() {
    try {
      loadingStore.doLoad()
      const product = productItem.value
      if (!product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.update(product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.save(product)
      }
      await getAll()
      closeDialog()
      loadingStore.finish()
    } catch (e: any) {
      // messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  const resetItem = () => {
    productItem.value = JSON.parse(JSON.stringify(initialProduct))
  }
  const editItem = async (item: Product) => {
    // productItem.id = item.id
    // productItem.name = item.name
    // productItem.price = item.price
    // productItem.category = item.category
    // console.log(productItem.category)
    openDialog()

    if (item.id) {
      await productService.getProduct(item.id)
      openDialog()
      console.log(getProduct(item.id))
    }
  }

  const deleteItem = async (item: Product) => {
    if (item.id) {
      await productService.delete(item.id)
      getAll()
    }
  }

  return {
    headers,
    items,
    productItem,
    dialog,
    drink,
    bakery,
    food,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll,
    getCategoryProducts
  }
})
