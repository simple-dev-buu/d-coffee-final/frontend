import type { Category } from './Category'

type Product = {
  id?: number
  name: string
  price: number
  category: Category
  image: string
}

function getImageUrl(product: Product) {
  return `http://localhost:3000/images/products/${product.image}`
}

export { type Product, getImageUrl }
