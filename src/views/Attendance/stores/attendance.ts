import { defineStore } from 'pinia'
import { computed, nextTick, ref } from 'vue'
import type { Attendance } from '../types/attendance'
import attendanceService from '@/services/attendance'
import { useAuthStore } from '@/stores/auth'

const dialogState = ref(false)

export const useAttendanceStore = defineStore('attendance', () => {
  const auth = useAuthStore()
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'CreatedDate', key: 'createdDate' },
    { title: 'Clock In', key: 'clockIn' },
    { title: 'Clock Out', key: 'clockOut' },
    { title: 'Types', key: 'type', sortable: false },
    { title: 'Time Worked', key: 'timeWorked', sortable: false },
    { title: 'Approved', key: 'approved', sortable: false }
  ]
  const chItem = ref<Attendance>({
    emp_ID: null,
    clockIn: '',
    timeWorked: 0,
    typeWork: null,
    approved: false
  })
  const initialAttendance: Attendance = {
    emp_ID: null,
    clockIn: '',
    timeWorked: 0,
    typeWork: null,
    approved: false
  }

  const resetItem = () => {
    chItem.value = JSON.parse(JSON.stringify(initialAttendance))
  }

  const timeClockIn = ref(0)
  const timeClockOut = ref(0)

  const items = ref<Attendance[]>()

  const getItems = computed(() => items.value)

  const actions = {
    saveClockIn() {
      timeClockIn.value = new Date().getTime()
      chItem.value.clockIn = new Date().toLocaleTimeString('th-TH', { hour12: false })
      chItem.value.emp_ID = auth.getCurrentUser()?.empId ?? 0
      console.log(chItem.value)
      saveClockInLocalStorage()
      attendanceService.createAttendance(chItem.value)
      nextTick(() => {
        getAll()
      })
    },
    async saveClockOut() {
      const clockIn: string | null = localStorage.getItem('clockIn')
      if (clockIn) {
        chItem.value = await attendanceService.getAttendanceByClockIn(clockIn)
        console.log(chItem.value)
        chItem.value.clockOut = new Date().toLocaleTimeString('th-TH', { hour12: false })
        const hoursClockIn: number = Number.parseInt(chItem.value.clockIn.slice(0, 2))
        const hoursClockOut: number = new Date().getHours()
        // const timeWorkedMilliseconds = Math.abs(timeClockIn.value - timeClockOut.value)
        const timeWorked = Math.floor(hoursClockOut - hoursClockIn)
        // chItem.value.timeWorked = Math.floor(timeWorkedMilliseconds / 3600000)
        chItem.value.timeWorked = timeWorked < 0 ? 0 : timeWorked
        chItem.value.typeWork = chItem.value.timeWorked >= 8 ? 'Normal' : 'Missing'
        attendanceService.updateAttendance(chItem.value)
        resetItem()
        nextTick(() => {
          getAll()
        })
      } else {
        alert('You need to check in first')
      }
    }
  }

  async function getAll() {
    items.value = await attendanceService.getAttendances()
  }

  const openDialog = () => {
    dialogState.value = true
  }

  const closeDialog = () => {
    dialogState.value = false
  }

  const saveClockInLocalStorage = async () => {
    localStorage.setItem(`clockIn`, chItem.value.clockIn)
  }

  const clockIn = () => {
    actions.saveClockIn()
    closeDialog()
  }
  const clockOut = () => {
    actions.saveClockOut()
    closeDialog()
  }
  return {
    items,
    getAll,
    dialogState,
    openDialog,
    closeDialog,
    headers,
    clockIn,
    clockOut,
    actions,
    chItem,
    timeClockIn,
    timeClockOut,
    getItems
  }
})
