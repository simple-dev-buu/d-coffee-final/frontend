type TypeWork = 'Missing' | 'Normal' | 'Normal/OT'
type Attendance = {
  id?: number
  emp_ID: null | number
  clockIn: string
  clockOut?: string | null
  timeWorked?: number
  typeWork?: TypeWork | null
  approved?: boolean
}

export type { TypeWork, Attendance }
