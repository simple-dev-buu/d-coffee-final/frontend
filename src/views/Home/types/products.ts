export interface ReportProducts {
  name: string
  quantity: string
  totalPrice: string
}
