export interface ReportIngredient {
  name: string
  totalOrdered: string
  totalCost: string
}
