export interface ExpenseSum {
  branch: string
  totalWorth: number
}

export interface ExpenseDetails {
  name: string
  totalWorth: string
}
