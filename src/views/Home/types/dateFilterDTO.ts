export interface DateFilterDTO {
  branchId?: number
  month: number
  year: number
}

export const defaultDateFilterDTO: DateFilterDTO = {
  month: 0,
  year: 0
}

export interface DateMaps {
  day: Day[]
  month: Month[]
  year: Year[]
}

export interface Day {
  net: number
  date: string
}

export interface Month {
  month: string
  net: number
}

export interface Year {
  year: string
  net: number
}
