import { BranchService } from '@/services/branch'
import { ReportService } from '@/services/report'
import { type Branch } from '@/views/Branch/types/branch'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { defaultDateFilterDTO, type DateMaps, type DateFilterDTO } from '../types/dateFilterDTO'
import type { ExpenseSum } from '../types/expense'

interface SalesData {
  dailySales: {
    yesterday: number | null
    today: number | null
  }
  monthlySales: {
    lastMonth: number | null
    thisMonth: number | null
  }
  yearlySales: {
    lastYear: number | null
    thisYear: number | null
  }
}

export const useHomeStore = defineStore('dashboard', {
  state() {
    return {
      thisYear: new Date().getFullYear(),
      thisMonth: new Date().getMonth() + 1,
      selectBranch: ref<Branch>(),
      salesData: ref<SalesData>(),
      availableBranch: ref<Branch[]>([]),
      revenueData: ref<DateMaps>(),
      expenseData: ref<DateMaps>(),
      revenueDTO: ref<DateFilterDTO>(defaultDateFilterDTO),
      expenseDTO: ref<DateFilterDTO>(defaultDateFilterDTO)
    }
  },
  getters: {
    getDailySales: (state) => state.salesData?.dailySales,
    getMonthlySales: (state) => state.salesData?.monthlySales,
    getYearlySales: (state) => state.salesData?.yearlySales,
    getBranch: (s) => s.availableBranch,
    getRevenueDay: (s) => s.revenueData?.day ?? [],
    getRevenueMonth: (s) => s.revenueData?.month ?? [],
    getRevenueYear: (s) => s.revenueData?.year ?? []
  },
  actions: {
    setupToday() {
      this.revenueDTO = {
        year: this.thisYear,
        month: this.thisMonth
      }
    },
    async fetchAll() {
      this.fetchRevenue()
      this.fetchSales()
      this.fetchBranch()
    },
    async fetchSales() {
      this.salesData = await ReportService.getSales()
    },
    async fetchBranch() {
      this.availableBranch = await BranchService.getAll()
    },
    async fetchBranchById(id: number) {
      this.selectBranch = await BranchService.getById(id)
    },
    async fetchSalesByBranchId() {
      if (this.selectBranch) {
        this.salesData = await ReportService.getSalesByBranchId(this.selectBranch?.id!)
      }
    },
    async fetchRevenue() {
      if (this.selectBranch) {
        this.revenueDTO.branchId = this.selectBranch.id
      }
      this.revenueData = await ReportService.getRevenue(this.revenueDTO)
    },
    async fetchExpense() {
      if (this.selectBranch) {
        this.expenseDTO.branchId = this.selectBranch.id
      }
      this.expenseData = await ReportService.getExpenseWithFilter(this.expenseDTO)
    },
    transformExpenseSum(items: ExpenseSum[]) {
      const result: { branch: string; totalWorth: number }[] = []

      items.forEach((item) => {
        if (item.branch) result.push({ branch: item.branch, totalWorth: item.totalWorth })
      })

      return result
    }
  }
})
