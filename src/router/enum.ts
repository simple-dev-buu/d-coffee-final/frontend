export const ROLE_ADMIN = 'admin'
export const ROLE_MANAGER = 'manager'
export const ROLE_CUSTOMER = 'customer'
export const ROLE_EMPLOYEE = 'employee'
export const ROLES_ANYONE = [ROLE_ADMIN, ROLE_EMPLOYEE, ROLE_MANAGER, ROLE_CUSTOMER]
export const ROLES_COMPANY = [ROLE_ADMIN, ROLE_EMPLOYEE, ROLE_MANAGER]
