import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/Home/views/HomeView.vue'
import { ROLES_ANYONE, ROLES_COMPANY, ROLE_ADMIN, ROLE_CUSTOMER, ROLE_MANAGER } from './enum'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: { requiresAuth: true, roles: [ROLE_ADMIN] }
    },
    {
      path: '/pos',
      name: 'pos',
      component: () => import('../views/pos/views/PosView.vue'),
      meta: { requiresAuth: true, roles: ROLES_COMPANY }
    },
    {
      path: '/product',
      name: 'product',
      component: () => import('../views/product/views/ProductView.vue'),
      meta: { requiresAuth: true, roles: [ROLE_ADMIN, ROLE_MANAGER] }
    },
    {
      path: '/promotion',
      name: 'promotion',
      component: () => import('../views/promotion/views/PromotionView.vue'),
      meta: { requiresAuth: true, roles: [ROLE_ADMIN, ROLE_MANAGER] }
    },
    {
      path: '/customer',
      name: 'customer',
      component: () => import('../views/customer/views/CustomerView.vue'),
      meta: { requiresAuth: true, roles: [ROLE_ADMIN, ROLE_MANAGER] }
    },
    {
      path: '/employee',
      name: 'employee',
      component: () => import('../views/employee/views/EmployeeView.vue'),
      meta: { requiresAuth: true, roles: [ROLE_ADMIN, ROLE_MANAGER] }
    },
    {
      path: '/inventory',
      name: 'inventory',
      component: () => import('../views/Inventory/views/InventoryView.vue'),
      meta: { requiresAuth: true, roles: ROLES_COMPANY }
    },
    {
      path: '/branch',
      name: 'branch',
      component: () => import('../views/Branch/views/BranchView.vue'),
      meta: { requiresAuth: true, roles: [ROLE_ADMIN] }
    },
    {
      path: '/ingredient',
      name: 'ingredient',
      component: () => import('../views/Ingredient/views/IngredientView.vue'),
      meta: { requiresAuth: true, roles: [ROLE_ADMIN] }
    },
    {
      path: '/replenishment-history',
      name: 'replenishment-history',
      component: () => import('../views/Inventory/views/ReplenishmentHistory.vue'),
      meta: { requiresAuth: true, roles: ROLES_COMPANY }
    },
    {
      path: '/stock-taking-history',
      name: 'stock-taking-history',
      component: () => import('../views/Inventory/views/StockTakingHistory.vue'),
      meta: { requiresAuth: true, roles: ROLES_COMPANY }
    },
    {
      path: '/order',
      name: 'order',
      component: () => import('../views/order/views/OrderView.vue'),
      meta: { requiresAuth: true, roles: ROLES_COMPANY }
    },
    {
      path: '/bill',
      name: 'bill',
      component: () => import('../views/bill/views/BillViews.vue'),
      meta: { requiresAuth: true, roles: ROLES_COMPANY }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/Auth/views/LoginView.vue'),
      meta: { requiresAuth: false }
    },
    {
      path: '/home-manager',
      name: 'Manager Home',
      component: () => import('../views/Home/views/ManagerView.vue'),
      meta: { requiresAuth: true, roles: [ROLE_ADMIN, ROLE_MANAGER] }
    },
    {
      path: '/home-customer',
      name: 'Customer Home',
      component: () => import('../views/CustomerHome/view/CustomerHome.vue'),
      meta: { requiresAuth: false }
    },
    {
      path: '/home-employee',
      name: 'employeeHome',
      component: () => import('../views/EmployeeHome/views/EmployeeHomeView.vue'),
      meta: { requiresAuth: true, roles: ROLES_COMPANY }
    },
    {
      path: '/payroll',
      name: 'payroll',
      component: () => import('../views/Payroll/views/PayrollView.vue'),
      meta: { requiresAuth: true, roles: [ROLE_ADMIN, ROLE_MANAGER] }
    },
    {
      path: '/user',
      name: 'user',
      component: () => import('../views/user/views/UserView.vue'),
      meta: { requiresAuth: true, roles: [ROLE_ADMIN] }
    },
    {
      path: '/attendance',
      name: 'attendance',
      component: () => import('../views/Attendance/views/AttendanceView.vue'),
      meta: { requiresAuth: true, roles: ROLES_COMPANY }
    },
    {
      path: '/forbidden',
      name: 'forbidden',
      component: () => import('../views/forbidden/views/ForbiddenView.vue'),
      meta: { requiresAuth: false }
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('../views/Profile/views/ProfileView.vue'),
      meta: { requiresAuth: true, roles: ROLES_ANYONE }
    }
  ]
})

export default router
