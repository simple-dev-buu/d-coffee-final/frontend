export function getDateNowString(): string {
  const timeZoneOffset = new Date().getTimezoneOffset() * 60000
  return new Date(Date.now() - timeZoneOffset).toISOString().slice(0, 10)
}

export function getTimeNowString(): string {
  return new Date().toLocaleTimeString('th-TH', { hour12: false })
}

export function formatLabel(key: string) {
  return key.replace(/([A-Z])/g, ' $1').replace(/^./, (str) => str.toUpperCase())
}
