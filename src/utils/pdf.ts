import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { font } from '@/assets/fonts/Kanit-normal'

export function generatePDF(
  titleHead: string,
  body: Array<string[]> | Array<any>,
  headers: Array<string>,
  filename: string,
  pdfFor?: string,
  otherDetail?: any
) {
  const doc = new jsPDF()

  const width = doc.internal.pageSize.getWidth()

  doc.addFileToVFS('Kanit-normal.ttf', font)
  doc.addFont('Kanit-normal.ttf', 'Kanit', 'normal')
  doc.setFont('Kanit', 'normal')
  console.log(doc.getFontList())
  doc.text(titleHead, width / 2, 10, { align: 'center' })

  if (pdfFor === 'payroll') {
    doc.text('Company Name: D-Coffee', 5, 20, { align: 'left' })
    doc.text(`Date: ${otherDetail?.createdDate}`, 5, 30, { align: 'left' })

    autoTable(doc, {
      head: [headers],
      margin: { top: 35 },
      body: body,
      styles: { font: 'Kanit' }
    })
  }
  
  if (!pdfFor) {
    autoTable(doc, {
      head: [headers],
      body: body,
      styles: { font: 'Kanit' }
    })
  }

  const existFilename = filename + '-' + new Date().toISOString() + '.pdf'

  doc.save(existFilename)
}
