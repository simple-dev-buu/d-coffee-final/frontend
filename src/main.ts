import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
// mdi-icon
import '@mdi/font/css/materialdesignicons.css'
// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { useAuthStore } from './stores/auth'
import { myCustomLightTheme, myCustomDarkTheme } from '@/assets/customTheme'

const vuetify = createVuetify({
  components,
  directives,
  theme: {
    defaultTheme: 'myCustomDarkTheme',
    themes: {
      myCustomLightTheme,
      myCustomDarkTheme
    }
  }
})

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(vuetify)

router.beforeEach((to, from, next) => {
  const authStore = useAuthStore()
  if (to.meta.requiresAuth) {
    if (!authStore.isAuthenticated()) {
      next({ name: 'login' })
    } else {
      const routeRoles = to.meta.roles as []
      if (!authStore.isAuthorized(routeRoles)) {
        next({ name: 'forbidden' })
      } else {
        next()
      }
    }
  } else {
    next()
  }
})

app.mount('#app')
