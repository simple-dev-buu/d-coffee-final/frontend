export interface NavListItem {
  title: string
  value: string
  path: string
  icon: string
}

export const navAdmin: NavListItem[] = [
  { title: 'Home', value: 'home', path: '/', icon: 'mdi-home' },
  { title: 'Branch Management', value: 'branch', path: '/branch', icon: 'mdi-store' },
  { title: 'Inventory Management', value: 'inventory', path: '/inventory', icon: 'mdi-warehouse' },
  { title: 'Ingredient Management', value: 'ingredient', path: '/ingredient', icon: 'mdi-coffee' },
  { title: 'Point of Sale', value: 'pos', path: '/pos', icon: 'mdi-point-of-sale' },
  { title: 'Product Management', value: 'product', path: '/product', icon: 'mdi-food' },
  { title: 'Promotion Management', value: 'promotion', path: '/promotion', icon: 'mdi-star' },
  {
    title: 'Customer Management',
    value: 'customer',
    path: '/customer',
    icon: 'mdi-wallet-membership'
  },
  {
    title: 'Employee Management',
    value: 'employee',
    path: '/employee',
    icon: 'mdi-account-box-multiple'
  },
  { title: 'User Management', value: 'user', path: '/user', icon: 'mdi-account' },
  { title: 'Receipt', value: 'order', path: '/order', icon: 'mdi-receipt-text' },
  { title: 'Attendance Management', value: 'attendance', path: '/attendance', icon: 'mdi-clock' },
  { title: 'Payroll Management', value: 'payroll', path: '/payroll', icon: 'mdi-currency-usd' },
  { title: 'Bill Management', value: 'bill', path: '/bill', icon: 'mdi-invoice-list' }
]

export const navManager: NavListItem[] = [
  { title: 'Home', value: 'home', path: '/home-manager', icon: 'mdi-home' },
  { title: 'Inventory Management', value: 'inventory', path: '/inventory', icon: 'mdi-warehouse' },
  { title: 'Point of Sale', value: 'pos', path: '/pos', icon: 'mdi-point-of-sale' },
  { title: 'Product Management', value: 'product', path: '/product', icon: 'mdi-food' },
  { title: 'Promotion Management', value: 'promotion', path: '/promotion', icon: 'mdi-star' },
  {
    title: 'Customer Management',
    value: 'customer',
    path: '/customer',
    icon: 'mdi-wallet-membership'
  },
  {
    title: 'Employee Management',
    value: 'employee',
    path: '/employee',
    icon: 'mdi-account-box-multiple'
  },
  { title: 'Receipt', value: 'order', path: '/order', icon: 'mdi-receipt-text' },
  { title: 'Attendance Management', value: 'attendance', path: '/attendance', icon: 'mdi-clock' },
  { title: 'Payroll Management', value: 'payroll', path: '/payroll', icon: 'mdi-currency-usd' },
  { title: 'Bill Management', value: 'bill', path: '/bill', icon: 'mdi-invoice-list' }
]

export const navEmployee: NavListItem[] = [
  { title: 'Home', value: 'home', path: '/home-employee', icon: 'mdi-home' },
  { title: 'Inventory Management', value: 'inventory', path: '/inventory', icon: 'mdi-warehouse' },
  { title: 'Point of Sale', value: 'pos', path: '/pos', icon: 'mdi-point-of-sale' },

  { title: 'Receipt', value: 'order', path: '/order', icon: 'mdi-receipt-text' },
  { title: 'Attendance Management', value: 'attendance', path: '/attendance', icon: 'mdi-clock' },
  { title: 'Bill Management', value: 'bill', path: '/bill', icon: 'mdi-invoice-list' }
]
