import type { ThemeDefinition } from 'vuetify'

export const myCustomLightTheme: ThemeDefinition = {
  dark: false,
  colors: {
    background: '#FFE878',
    surface: '#C6A054',
    'surface-bright': '#D5B869',
    'surface-light': '#FFFFFF',
    'surface-variant': '#393939',
    'on-surface-variant': '#010101',
    primary: '#5B4021',
    'primary-darken-1': ' #221E19',
    secondary: '#82552C',
    'secondary-darken-1': '#4E2F1C',
    error: '#FF0018',
    info: '#0C00BB',
    success: '#008018',
    warning: '#FF0018'
  },
  variables: {
    'border-color': '#000000',
    'border-opacity': 0.12,
    'high-emphasis-opacity': 0.87,
    'medium-emphasis-opacity': 0.6,
    'disabled-opacity': 0.38,
    'idle-opacity': 0.04,
    'hover-opacity': 0.04,
    'focus-opacity': 0.12,
    'selected-opacity': 0.08,
    'activated-opacity': 0.12,
    'pressed-opacity': 0.12,
    'dragged-opacity': 0.08,
    'theme-kbd': '#212529',
    'theme-on-kbd': '#FFFFFF',
    'theme-code': '#F5F5F5',
    'theme-on-code': '#000000'
  }
}

export const myCustomDarkTheme = {
  dark: true,
  colors: {
    background: '#1A1A1A',
    surface: '#292929',
    'surface-variant': '#3D3D3D',
    'on-surface-variant': '#CCCCCC',
    primary: '#9C6A42',
    'primary-darken-1': '#7B5435',
    secondary: '#8D6E63',
    'secondary-darken-1': '#6D4C41',
    error: '#B71C1C',
    info: '#4A6F8A',
    success: '#5D8C68',
    warning: '#C88719'
  },
  variables: {
    'border-color': '#000000',
    'border-opacity': 0.12,
    'high-emphasis-opacity': 0.87,
    'medium-emphasis-opacity': 0.6,
    'disabled-opacity': 0.38,
    'idle-opacity': 0.04,
    'hover-opacity': 0.04,
    'focus-opacity': 0.12,
    'selected-opacity': 0.08,
    'activated-opacity': 0.12,
    'pressed-opacity': 0.12,
    'dragged-opacity': 0.08,
    'theme-kbd': '#212529',
    'theme-on-kbd': '#FFFFFF',
    'theme-code': '#F5F5F5',
    'theme-on-code': '#000000'
  }
}
