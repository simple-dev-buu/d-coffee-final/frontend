import type { Replenishment, ReplenishmentItem } from '@/views/Inventory/types/replenishment'
import http from './http'

export class ReplenishmentService {
  private static path = '/replenishment'
  public static async getAll() {
    try {
      const res = await http.get(this.path)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public static async getAllWithFilter(year: number, month: number) {
    try {
      const res = await http.get(`${this.path}/${year}/${month}`)
      return res.data
    } catch(e) {
      console.error(e)
    }
  }
  public static async create(item: Replenishment) {
    try {
      const res = await http.post(this.path, item)
      return res.status
    } catch(e) {
      console.error(e)
    }
  }
  public static async delete(index: number) {
    try {
      const res = await http.delete(`${this.path}/${index}`)
      return res.data
    } catch (e) {
      console.error(e)   
    }

  }
  public static async getById(index: number) {
    try {
      const res = await http.get(`${this.path}/${index}`)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public static async update(item: Replenishment) {
    try {
      const res = await http.patch(`${this.path}/${item.id}`, item)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public static async updateAll(items: ReplenishmentItem[]) {
    try {
      const res = await http.patch(`${this.path}`, items)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
}
