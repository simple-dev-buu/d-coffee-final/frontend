import type { Bill } from '@/views/bill/types/Bill'
import http from './http'

export class BillsService {
  private static path = '/bills'
  public static async getAll() {
    try {
      const res = await http.get(this.path)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }

  public static async getAllWithFilter(year: number, month: number) {
    try {
      const res = await http.get(`${this.path}/${year}/${month}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }

  public static async getSumWithFilter(name: string, year:number, month: number) {
    try {
      const res = await http.get(`${this.path}/sum/${year}/${month}/${name}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }

  public static async findBillThisMonth(
    billType: string,
    targetMonth: number,
    specificBranch: number,
    targeYear: number,
  ) {
    try {
      const data = {
        billType: '',
        targetMonth: 0,
        specificBranch: 0,
        targetYear: 0
      }
      data.billType = billType
      data.targetMonth = targetMonth
      data.specificBranch = specificBranch
      data.targetYear = targeYear
      const res = await http.post(this.path + '/query', data)
      console.log("res:" + res.data)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }

  public static async create(item: Bill) {
    try {
      const res = await http.post(this.path, item)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }

  public static async delete(index: number) {
    try {
      const res = await http.delete(`${this.path}/${index}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async getById(index: number) {
    try {
      const res = await http.get(`${this.path}/${index}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async update(item: Bill) {
    try {
      const res = await http.patch(`${this.path}/${item.id}`, item)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
}
