import { useAuthStore } from '@/stores/auth'
import { useLoadingStore } from '@/stores/loading'
import { useMessageStore } from '@/stores/message'
import axios from 'axios'
import { useRouter } from 'vue-router'

export const baseURL = 'http://localhost:3000'
export const baseUrlImage = `${baseURL}/images`

const instance = axios.create({
  baseURL: baseURL
})

function delay(sec: number) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(sec), sec * 1000)
  })
}
instance.interceptors.request.use(
  async function (config) {
    const loader = useLoadingStore()
    loader.isLoading = true
    await delay(0.7)
    loader.isLoading = false
    const token = localStorage.getItem('access_token')
    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  async function (res) {
    return res
  },
  function (err) {
    const originalConfig = err.config

    if (originalConfig.url !== '/auth/login' && err.response) {
      // Access Token was expired
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true
        try {
          useAuthStore().logout(true)
          useMessageStore().showMessage('Token was expired', 'info')
          return instance(originalConfig)
        } catch (_error) {
          return Promise.reject(_error)
        }
      }
    }
    if (err.code === 'ERR_NETWORK') {
      try {
        useMessageStore().showMessage('No network connection', 'info')
        useRouter().replace('/login')
        return Promise.reject(err)
      } catch {
        throw Error
      }
    }
    return Promise.reject(err)
  }
)
export default instance
