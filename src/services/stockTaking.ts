import type { StockTaking } from '@/views/Inventory/types/stockTaking'
import http from './http'

export class StockTakingService {
  private static path = '/stock-taking'
  public static async getAll() {
    try {
      const res = await http.get(this.path)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public static async getAllWithFilter(year: number, month: number) {
    try {
      const res = await http.get(`${this.path}/${year}/${month}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async create(item: StockTaking) {
    try {
      const res = await http.post(this.path, item)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public static async delete(index: number) {
    try {
      const res = await http.delete(`${this.path}/${index}`)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }

  public static async getOne(id: number) {
    try {
      const res = await http.get(`${this.path}/${id}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
}
