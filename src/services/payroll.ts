import http from '@/services/http'
import type { Payroll } from '@/views/Payroll/types/Payroll'

async function savePayroll(payroll: Payroll[]) {
  try {
    console.log(payroll)
    return await http.post('/payrolls', payroll)
  } catch (e) {
    console.error(e)
  }

}

async function getPayrolls() {
  try {
    return await http.get('/payrolls')
  } catch (e) {
    console.error(e)
  }

}

async function getAllEmployee() {
  try {
    return await http.get('/employees')
  } catch (e) {
    console.error(e)
  }

}

async function getPayroll(id: number) {
  try {
    return await http.get(`/payrolls/${id}`)
  } catch (e) {
    console.error(e)
  }

}

async function updatePayroll(payroll: Payroll) {
  try {
    return await http.put(`/payrolls/${payroll.id}`, payroll)
  } catch (e) {
    console.error(e)
  }

}

async function deletePayroll(payroll: Payroll) {
  try {
    return await http.delete(`/payrolls/${payroll.id}`)
  } catch (e) {
    console.error(e)
  }

}

export default {
  savePayroll,
  getPayrolls,
  getPayroll,
  updatePayroll,
  deletePayroll,
  getAllEmployee
}
