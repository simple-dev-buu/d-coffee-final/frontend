import http from '@/services/http'
import type { Receipt } from '../views/receipt/types/receipt'
import type { ReceiptItem } from '../views/receipt/types/receiptItem'

type ReceiptDto = {
  orderItems: {
    productId: number
    name: string
    price: number
    unit: number
  }[]
  empId: number
  branchId: number
  customerId: number
  promotionId: number
  receivedAmount: number
  paymentType: string
  usePoint: number
  givePoint: number
  totalPoint: number
}

type DataDto = {
  money: number
  branchId: number
}

function addReceipt(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const receiptDto: ReceiptDto = {
    orderItems: [],
    empId: 0,
    branchId: 0,
    customerId: 0,
    promotionId: 0,
    receivedAmount: 0,
    paymentType: '',
    usePoint: 0,
    givePoint: 0,
    totalPoint: 0
  }
  receiptDto.empId = receipt.empId
  receiptDto.branchId = receipt.branchId
  receiptDto.promotionId = -1
  receiptDto.receivedAmount = receipt.receivedAmount
  receiptDto.paymentType = receipt.paymentType
  receiptDto.usePoint = receipt.usePoint
  receiptDto.givePoint = receipt.givePoint
  receiptDto.totalPoint = receipt.totalPoint
  receiptDto.orderItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      name: item.name,
      price: item.price,
      unit: item.unit
    }
  })
  console.log(receiptDto)
  return http.post('/orders', receiptDto)
}

async function getMore(money: number, branchId: number) {
  const data: DataDto = {
    money: 0,
    branchId: 0
  }
  data.money = money
  data.branchId = branchId
  const res = await http.post(`/receipts/more`, data)
  return res.data
}

async function getLess(money: number, branchId: number) {
  const data: DataDto = {
    money: 0,
    branchId: 0
  }

  data.money = money
  data.branchId = branchId
  const res = await http.post(`/receipts/less`, data)
  return res.data
}

export default { addReceipt, getMore, getLess }
