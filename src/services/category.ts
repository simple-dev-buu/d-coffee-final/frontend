import type { Category } from '@/views/product/types/Category'
import http from '@/services/http'

function addCategory(category: Category) {
  try {
    return http.post('/categories', category)
  } catch (e) {
    console.error(e)
  }
}

function updateCategory(category: Category) {
  try {
    return http.patch(`/categories/${category.id}`, category)
  } catch (e) {
    console.error(e)
  }
}

function delCategory(category: Category) {
  try {
    return http.delete(`/categories/${category.id}`)
  } catch (e) {
    console.error(e)
  }
}

function getCategory(id: number) {
  try {
    return http.get(`/categories/${id}`)
  } catch (e) {
    console.error(e)
  }
}

function getCategories() {
  try {
    return http.get('/categories')
  } catch (e) {
    console.error(e)
  }
}

export default { addCategory, updateCategory, delCategory, getCategory, getCategories }
