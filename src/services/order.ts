import type { Receipt } from '@/views/receipt/types/receipt'
import type { ReceiptItem } from '@/views/receipt/types/receiptItem'
import http from '@/services/http'

type ReceiptDto = {
  receiptItems: {
    productId: number
    name: string
    price: number
    unit: number
    total: number
  }[]
  userId: number
  branchId: number
  customerId: number
  promotionId: number

  totalUnit: number
  totalBefore: number
  discount: number
  netPrice: number
  receivedAmount: number
  change: number
  paymentType: string
  givePoint: number
  usePoint: number
  totalPoint: number
}

async function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const receiptDto: ReceiptDto = {
    receiptItems: [],
    userId: 0,
    branchId: 0,
    customerId: 0,
    promotionId: 0,
    receivedAmount: 0,
    paymentType: '',
    usePoint: 0,
    givePoint: 0,
    totalPoint: 0,
    totalUnit: 0,
    totalBefore: 0,
    discount: 0,
    netPrice: 0,
    change: 0
  }
  receiptDto.userId = receipt.empId
  receiptDto.branchId = 1
  receiptDto.customerId = receipt.cusId
  receiptDto.promotionId = -1
  receiptDto.receivedAmount = receipt.receivedAmount
  receiptDto.totalUnit = receipt.totalUnit
  receiptDto.totalBefore = receipt.totalBefore
  receiptDto.paymentType = receipt.paymentType
  receiptDto.usePoint = receipt.usePoint
  receiptDto.discount = receipt.discount
  receiptDto.givePoint = receipt.givePoint
  receiptDto.totalPoint = receipt.totalPoint
  receiptDto.netPrice = receipt.netPrice
  receiptDto.change = receipt.change

  receiptDto.receiptItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      name: item.name,
      price: item.price,
      unit: item.unit,
      total: item.unit * item.price
    }
  })

  console.log(receiptDto)
  console.log(receiptDto.customerId)
  alert('test')
  // const res = await http.post('/receipts', receiptDto)
  // return res.data
}

async function getAll() {
  try {
    const res = await http.get('/receipts')
    return res.data
  } catch (e) {
    console.error(e)
  }

}

async function getMore(money: number) {
  try {
    const res = await http.get(`/receipts/more/:${money}`)
    return res.data
  } catch (e) {
    console.error(e)
  }

}

async function getLess(money: number) {
  try {
    const res = await http.get(`/receipts/less/:${money}`)
    return res.data
  } catch (e) {
    console.error(e)
  }

}

export default { addOrder, getAll }
