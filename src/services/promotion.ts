import type { Promotion } from '@/views/promotion/types/Promotion'
import http from './http'

export class PromotionService {
  private static path = '/promotions'
  public static async getAll() {
    try {
      const res = await http.get(this.path)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }

  public async save(item: Promotion) {
    const { files, ...rest } = item
    const formData = new FormData()
    formData.append('promotion', JSON.stringify(rest))
    if (item.files && item.files.length > 0) {
      formData.append('file', files![0])
    }
    console.log(formData.values)
    return http.post(`/promotions`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
  public async update(item: Promotion) {
    // await http.patch('/promotion', item)
    // const formData = new FormData()
    // formData.append('name', item.name)
    // formData.append('description', item.description)
    // formData.append('discount', item.discount.toString())
    // formData.append('comboSet', item.comboSet.valueOf.toString())
    // formData.append('startDate', item.startDate)
    // formData.append('endDate', item.endDate)
    // formData.append('status', item.status.valueOf.toString())

    // if (item.files && item.files.length > 0) formData.append('file', item.files[0])
    // return http.post(`/promotions/${item.id}`, formData, {
    //   headers: {
    //     'Content-Type': 'multipart/form-data'
    //   }
    // })
    const { files, ...rest } = item
    const formData = new FormData()
    formData.append('promotion', JSON.stringify(rest))
    if (item.files && item.files.length > 0) {
      formData.append('file', files![0])
    }
    console.log(formData.values)
    return http.post(`/promotions/${item.id}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
  public async getAll() {
    try {
      const res = await http.get('/promotions')
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  //   public async getPromotionByType(categoryId: number) {
  //     const res = await http.get(`/promotions/category/` + categoryId)
  //     return res.data
  //   }
  public async getPromotion(id: number) {
    try {
      return await http.get(`/promotions/${id}`)
    } catch (e) {
      console.error(e)
    }

  }
  public async delete(index: number) {
    try {
      await await http.delete(`/promotions/${index}`)
    } catch (e) {
      console.error(e)
    }

  }
}
