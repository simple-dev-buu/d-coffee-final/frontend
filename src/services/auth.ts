import type { AxiosError } from 'axios'
import http from './http'
import { useMessageStore } from '@/stores/message'

export class AuthService {
  private static readonly path = '/auth'
  private static msgStore = useMessageStore
  public static async signIn(email: string, password: string) {
    try {
      const dto = { email: email, password: password }
      const res = await http.post(`${this.path}/login`, dto)
      return res.data
    } catch (error: any) {
      if (error.isAxiosError) {
        const axiosError = error as AxiosError<any>
        if (axiosError.response) {
          const { data } = axiosError.response
          this.msgStore().showMessage(data.message, 'error')
        }
      }
      throw new Error('An error occurred during sign-in')
    }
  }
}
