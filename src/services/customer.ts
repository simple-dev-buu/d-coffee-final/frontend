import http from '@/services/http'
import type { Customer } from '@/views/customer/types/Customer'

export class CustomersService {
  public async save(item: Customer & { files: File[] }) {
    const formData = new FormData()
    // formData.append('name', item.name)
    // formData.append('tel', item.tel)
    // formData.append('birthDay', JSON.stringify(item.birthDay))
    formData.append('customer', JSON.stringify(item))

    if (item.files && item.files.length > 0) {
      formData.append('file', item.files[0])
    }
    console.log('From Service' + formData)
    const res = await http.post(`/customers`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    return res.data
  }
  public async update(item: Customer & { files: File[] }) {
    const formData = new FormData()
    formData.append('name', item.name)
    formData.append('tel', item.tel)
    formData.append('birthDay', JSON.stringify(item.birthDay))
    formData.append('point', item.point.toString())
    formData.append('status', JSON.stringify(item.status))

    if (item.files && item.files.length > 0) formData.append('file', item.files[0])
    console.log(formData)
    return http.post(`/customers/${item.id}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }

  public async getAll() {
    try {
      const res = await http.get('/customers')
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public async getByTel(tel: string) {
    try {
      const res = await http.get('/customers/telephone/' + tel)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }

  public async getCustomer(id: number) {
    try {
      const res = await http.get(`/customers/${id}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public async delete(index: number) {
    try {
      await http.delete(`/customers/${index}`)
    } catch (e) {
      console.error(e)
    }
  }
}

function updateCustomer(customer: Customer) {
  try {
    return http.patch(`/customers/${customer.id}`, customer)
  } catch (e) {
    console.error(e)
  }
}

type DataDto = {
  month: number
  empId: number
  year: number
}

function delCustomer(customer: Customer) {
  try {
    return http.delete(`/customers/${customer.id}`)
  } catch (e) {
    console.error(e)
  }
}

function getCustomer(id: number) {
  try {
    return http.get(`/customers/${id}`)
  } catch (e) {
    console.error(e)
  }
}

function getAllCustomer() {
  try {
    return http.get('/customers')
  } catch (e) {
    console.error(e)
  }
}

// import http from '@/services/http'
// import type { Customer } from '@/views/customer/types/Customer'
// class CustomerService {
//   public async save(item: Customer & { files: File[] }) {
//     // await http.post('/employees', item)
//     const formData = new FormData()
//     formData.append('name', item.name)
//     formData.append('tel', item.tel)
//     formData.append('birthDay', item.birthDay.toISOString().split('T')[0])
//     formData.append('point', item.point.toString())
//     formData.append('registerDate', item.registerDate.toISOString().split('T')[0])

//     if (item.files && item.files.length > 0) formData.append('file', item.files[0])
//     return http.post(`/customers/${item.id}`, formData, {
//       headers: {
//         'Content-Type': 'multipart/form-data'
//       }
//     })
//   }
//   public async update(item: Customer & { files: File[] }) {
//     // await http.patch('/employees', item)
//     const formData = new FormData()
//     formData.append('name', item.name)
//     formData.append('tel', item.tel)
//     formData.append('birthDay', item.birthDay.toISOString().split('T')[0])
//     formData.append('point', item.point.toString())
//     formData.append('registerDate', item.registerDate.toISOString().split('T')[0])
//     if (item.files && item.files.length > 0) formData.append('file', item.files[0])
//     return http.post('/customers', formData, {
//       headers: {
//         'Content-Type': 'multipart/form-data'
//       }
//     })
//   }

//   public async getAll() {
//     const res = await http.get('/customers')
//     return res.data
//   }

//   public async getById(id: number): Promise<Customer> {
//     const res = await http.get('/customers/' + id)
//     return res.data
//   }

//   public async getByTel(tel: string): Promise<Customer> {
//     const res = await http.get('/customers/telephone/' + tel)
//     return res.data
//   }
//   public async delete(index: number) {
//     await http.delete(`/customers/${index}`)
//   }
// }

// function updateCustomer(customer: Customer) {
//   return http.patch(`/customers/${customer.id}`, customer)
// }

// type DataDto = {
//   month: number
//   empId: number
//   year: number
// }

// function delCustomer(customer: Customer) {
//   return http.delete(`/customers/${customer.id}`)
// }

// function getCustomer(id: number) {
//   return http.get(`/customers/${id}`)
// }

// function getAllCustomer() {
//   return http.get('/customers')
// }

// export default CustomerService

// import http from '@/services/http'
// import type { Customer } from '@/views/customer/types/Customer'

// class CustomerService {
//   public async save(item: Customer) {
//     const res = await http.post('/customers/query', item)
//     return res.data
//     // const formData = new FormData()
//     // formData.append('name', item.name)
//     // formData.append('tel', item.tel)
//     // formData.append('birthDay', item.birthDay.toISOString().split('T')[0])
//     // formData.append('image', 'noimage.jpg')
//     // return http.post(`/customers/query`, formData, {
//     //   headers: {
//     //     'Content-Type': 'multipart/form-data'
//     //   }
//     // })
//   }
//   public async saveForPosPage(item: Customer) {
//     const res = await http.post('/customers/query', item)
//     return res.data
//     // const formData = new FormData()
//     // formData.append('name', item.name)
//     // formData.append('tel', item.tel)
//     // formData.append('birthDay', item.birthDay.toISOString().split('T')[0])
//     // formData.append('image', 'noimage.jpg')
//     // return http.post(`/customers/query`, formData, {
//     //   headers: {
//     //     'Content-Type': 'multipart/form-data'
//     //   }
//     // })
//   }
//   public async update(item: Customer) {
//     await http.patch(`/customers/${item.id}`, item)
//   }
//   public async getAll() {
//     const res = await http.get('/customers')
//     return res.data
//   }

// public async getByTel(tel: string): Promise<Customer> {
//   const res = await http.get('/customers/telephone/' + tel)
//   return res.data
// }

//   public async delete(index: number) {
//     await http.delete(`/customers/${index}`)
//   }
// }

// export default CustomerService
