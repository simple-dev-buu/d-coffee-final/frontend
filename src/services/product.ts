import type { Product } from '@/views/product/types/Product'
import http from '@/services/http'

class ProductService {
  public async save(item: Product & { files: File[] }) {
    // await http.post('/products', item)
    const formData = new FormData()
    formData.append('name', item.name)
    formData.append('price', item.price.toString())
    formData.append('category', JSON.stringify(item.category))
    if (item.files && item.files.length > 0) formData.append('file', item.files[0])
    return http.post(`/products/${item.id}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
  public async update(item: Product & { files: File[] }) {
    // await http.patch('/products', item)
    const formData = new FormData()
    formData.append('name', item.name)
    formData.append('price', item.price.toString())
    formData.append('category', JSON.stringify(item.category))
    if (item.files && item.files.length > 0) formData.append('file', item.files[0])
    return http.post('/products', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
  public async getAll() {
    try {
      const res = await http.get('/products')
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public async getProductByType(categoryId: number) {
    try {
      const res = await http.get(`/products/category/` + categoryId)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public async getProduct(id: number) {
    try {
      return await http.get(`/products/${id}`)
    } catch (e) {
      console.error(e)
    }

  }
  public async delete(index: number) {
    try {
      await await http.delete(`/products/${index}`)
    } catch (e) {
      console.error(e)
    }

  }
}

export default ProductService
