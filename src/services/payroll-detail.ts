import http from '@/services/http'
import type { Payroll } from '@/views/Payroll/types/Payroll'
// import type { PayrollDetail } from '@/views/Payroll/types/PayrollDetail'

async function savePayrollDetail(payroll: Payroll) {
  try {
    return await http.post('/payroll-detail', payroll)
  } catch (e) {
    console.error(e)
  }

}

async function getPayrollsDetail() {
  try {
    return await http.get('/payrolls')
  } catch (e) {
    console.error(e)
  }

}

async function getPayrollDetail(id: number) {
  try {
    return await http.get(`/payrolls/${id}`)
  } catch (e) {
    console.error(e)
  }

}

async function updatePayrollDetail(payroll: Payroll) {
  try {
    return await http.put(`/payrolls/${payroll.id}`, payroll)
  } catch (e) {
    console.error(e)
  }

}

async function deletePayrollDetail(payroll: Payroll) {
  try {
    return await http.delete(`/payrolls/${payroll.id}`)
  } catch (e) {
    console.error(e)
  }

}

export default {
  savePayrollDetail,
  getPayrollsDetail,
  getPayrollDetail,
  updatePayrollDetail,
  deletePayrollDetail
}
