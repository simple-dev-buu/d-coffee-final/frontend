import http from '@/services/http'
import type { Employee } from '@/views/employee/types/Employee'

// function addEmployee(employee: Employee) {
//   return http.post('/employees', employee)
// }

// function updateEmployee(employee: Employee) {
//   return http.patch(`/employees/${employee.id}`, employee)
// }

// function delEmployee(employee: Employee) {
//   return http.delete(`/employees/${employee.id}`)
// }

// function getEmployee(id: number) {
//   return http.get(`/employees/${id}`)
// }

// function getAllEmployee() {
//   return http.get('/employees')
// }
export class EmployeeService {
  public static async save(item: Employee & { files: File[] }) {
    // await http.post('/employees', item)
    const formData = new FormData()
    formData.append('firstName', item.firstName)
    formData.append('lastName', item.lastName)
    formData.append('tel', item.tel)
    formData.append('gender', item.gender)
    if (item.birthDay !== null) {
      formData.append('birthDay', item.birthDay.toString())
    }
    formData.append('title', item.title)
    formData.append('qualification', item.qualification)
    formData.append('moneyRate', item.moneyRate.toString())
    formData.append('minWork', item.minWork.toString())
    formData.append('startDate', item.startDate.toString())
    if (item.endDate !== null) {
      formData.append('endDate', item.endDate.toString())
    }

    if (item.files && item.files.length > 0) formData.append('file', item.files[0])
    return http.post(`/employees/${item.id}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
  public static async update(item: Employee & { files: File[] }) {
    // await http.patch('/employees', item)
    const formData = new FormData()
    formData.append('firstName', item.firstName)
    formData.append('lastName', item.lastName)
    formData.append('tel', item.tel)
    formData.append('gender', item.gender)
    if (item.birthDay !== null) {
      formData.append('birthDay', item.birthDay.toString())
    }
    formData.append('title', item.title)
    formData.append('qualification', item.qualification)
    formData.append('moneyRate', item.moneyRate.toString())
    formData.append('minWork', item.minWork.toString())
    formData.append('startDate', item.startDate.toString())
    if (item.endDate !== null) {
      formData.append('endDate', item.endDate.toString())
    }

    if (item.files && item.files.length > 0) formData.append('file', item.files[0])
    return http.post('/employees', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }

  public static async getAll() {
    try {
      const res = await http.get('/employees')
      return res.data
    } catch (e) {
      console.error(e)
    }

  }

  public static async getEmployee(id: number) {
      return http.get(`/employees/${id}`)

  }
  public static async delete(index: number) {
    try {
      await http.delete(`/employees/${index}`)
    } catch (e) {
      console.error(e)
    }
  }
  public static async getAttendence(month: number, empId: number, year: number) {
    try {
      const data: DataDto = {
        month: 0,
        empId: 0,
        year: 0
      }
      data.month = month
      data.empId = empId
      data.year = year
      const res = await http.post(`/employees/query`, data)
      return res.data
    } catch (e) {
      console.error(e)
    }
    
  }
}

function updateEmployee(employee: Employee) {
  try {
    return http.patch(`/employees/${employee.id}`, employee)
  } catch (e) {
    console.error(e)
  }
}

type DataDto = {
  month: number
  empId: number
  year: number
}

function delEmployee(employee: Employee) {
  try {
    return http.delete(`/employees/${employee.id}`)

  } catch (e) {
    console.error(e)
  }
}

function getEmployee(id: number) {
  try {
    return http.get(`/employees/${id}`)

  } catch (e) {
    console.error(e)
  }
}

function getAllEmployee() {
  try {
    return http.get('/employees')
  } catch (e) {
    console.error(e)
  }

}

export default EmployeeService
