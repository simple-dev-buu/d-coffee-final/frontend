import http from '@/services/http'
import type { Attendance } from '@/views/Attendance/types/attendance'

async function createAttendance(attendance: Attendance) {
  try {
    return await http.post(`/attendances`, attendance)
  } catch (e) {
    console.error(e)
  }
}

function updateAttendance(attendance: Attendance) {
  console.log("In Update attendance Service Front")
  try {
    return http.patch(`/attendances/${attendance.id}`, attendance)
  } catch (e) {
    console.error(e)
  }
}

function getAttendance(id: number) {
  try {
    return http.get(`/attendances/${id}`)
  } catch (e) {
    console.error(e)
  }
}

async function getAttendances() {
  try {
    const res = await http.get(`/attendances`)
    return res.data
  } catch (e) {
    console.error(e)
  }
}

async function getAttendanceByClockIn(clockIn: string | null) {
  try {
    const res = await http.get(`/attendances/clockIn/${clockIn}`)
  return res.data
  } catch (e) {
    console.error(e)
  }
}

export default { createAttendance, updateAttendance, getAttendance, getAttendances, getAttendanceByClockIn }
