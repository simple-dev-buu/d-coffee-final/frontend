import type { Ingredient } from '@/views/Ingredient/types/ingredient'
import http from './http'

export class IngredientService {
  private static path = '/ingredients'
  public static async getAll() {
    try {
      const res = await http.get(this.path)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async create(item: Ingredient) {
    try {
      const res = await http.post(this.path, item)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public static async delete(index: number) {
    try {
      const res = await http.delete(`${this.path}/${index}`)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public static async getById(index: number) {
    try {
      const res = await http.get(`${this.path}/${index}`)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
  public static async update(item: Ingredient) {
    try {
      const res = await http.patch(`${this.path}/${item.id}`, item)
      return res.data
    } catch (e) {
      console.error(e)
    }

  }
}
