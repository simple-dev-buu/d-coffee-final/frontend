import type { DateFilterDTO } from '@/views/Home/types/dateFilterDTO'
import http from './http'

export class ReportService {
  private static path = 'reports'
  public static async getFull(branch: number, year: number, month: number) {
    try {
      const res = await http.get(`${this.path}/${branch}/${year}/${month}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }

  public static async getSales() {
    try {
      const res = await http.get(`${this.path}/sales`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }

  public static async getSalesByBranchId(id: number) {
    try {
      const res = await http.get(`${this.path}/sales/${id}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async getRevenue(data: DateFilterDTO) {
    try {
      const res = await http.post(`${this.path}/revenue`, data)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async getExpenseWithFilter(data: DateFilterDTO) {
    try {
      const res = await http.post(`${this.path}/expense`, data)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async getExpenseSum() {
    try {
      const res = await http.get(`${this.path}/expense-sum`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async getExpenseBranch(id: number) {
    try {
      const res = await http.post(`${this.path}/expense-branch/${id}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async getTopProducts(index: number) {
    try {
      const res = await http.post(`${this.path}/top-products/${index}`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
  public static async getIngredients() {
    try {
      const res = await http.get(`${this.path}/ingredients`)
      return res.data
    } catch (e) {
      console.error(e)
    }
  }
}
