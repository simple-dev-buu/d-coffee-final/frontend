import { defineStore } from 'pinia'
import { ref } from 'vue'

// export const useLoadingStore = defineStore('loading', {
//   state() {
//     return {
//       isLoading: ref(false)
//     }
//   }
// })

export const useLoadingStore = defineStore('loading', () => {
  const isLoading = ref(false)
  const doLoad = () => {
    isLoading.value = true
  }
  const finish = () => {
    isLoading.value = false
  }
  return { isLoading, doLoad, finish }
})
