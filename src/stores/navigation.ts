import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useNavStore = defineStore('navigation', {
  state() {
    return {
      showDrawer: ref<boolean>(true),
      showAppBar: ref<boolean>(true)
    }
  },
  getters: {
    getMainDrawer: (s) => {
      return s.showDrawer
    }
  },
  actions: {
    switchDrawer() {
      this.showDrawer = !this.showDrawer
    },
    hideNavLayout() {
      this.showAppBar = false
      this.showDrawer = false
    },
    showNavLayout() {
      this.showAppBar = true
      this.showDrawer = true
    }
  }
})
