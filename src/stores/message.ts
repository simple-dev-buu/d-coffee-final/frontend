import { ref } from 'vue'
import { defineStore } from 'pinia'

type TypeMSG = 'info' | 'success' | 'error'
export const useMessageStore = defineStore('message', () => {
  const snackbar = ref(false)
  const text = ref('')
  const typeMSG = ref<TypeMSG>('info')
  const showMessage = function (msg: string, type?: TypeMSG) {
    text.value = msg
    snackbar.value = true
    typeMSG.value = type ?? 'info'
  }

  return { showMessage, snackbar, text, typeMSG }
})
